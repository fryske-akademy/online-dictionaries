# docker setup for Json dictionary services

### create file 'adminpw' to change admin password during build:
```
sm:passwd('admin','xxxx')
```
## environment
```bash
export APPNAME=xxx
export VERSION=xxx
export VOLUMEROOT=${HOME}/volumes
export DOCKER_BUILDKIT=1
```
## prepare application (.xar)
- build xar: ```cd jsonDictionary; ant```
- copy xar to the docker directory
- ```mkdir -p ${VOLUMEROOT}/${APPNAME}json/config```

## build image:
In the Dockerfile you find optional arguments. Dictionary data are imported during build.
```
mkdir data
either copy dictionary files to the data directory or "sudo mount --bind ${VOLUMEROOT}/${APPNAME}json/data data"
docker build --secret id=adminpw,src=adminpw -t ${APPNAME}:${VERSION} .
optionaly "sudo umount data"
rm adminpw
```
This build will have the data indexed in the image and the admin password changed.

## Syncing data

Dictionary data can be synced from a directory tree into a collection. The setup described here does a one time sync
during build. This approach results in a possibly large image containing data and indexes,
while it reduces startup time of containers. When your dictionary data is edited you may need a periodic sync, it
can be uncommented in conf.xml.
- see also [exist-db add-on](https://bitbucket.org/fryske-akademy/exist-db-addons)

## run image
```
#once:
cp teidictjson.properties conf.xml ${VOLUMEROOT}/${APPNAME}json/config/
#adapt the config files (conf.xml contains commented cron job for DataSync)
docker secret create ${APPNAME}json.properties ${VOLUMEROOT}/${APPNAME}json/config/teidictjson.properties
docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
#start:
docker stack deploy --compose-file docker-compose.yml ${APPNAME}
```

### usefull commands

```
docker container|service|image|stack ls
docker service|container logs (-f) ${APPNAME}
docker stack rm ${APPNAME}
docker exec -it <container> "/bin/bash"
```
