<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns="http://www.tei-c.org/ns/1.0"
                xmlns:tolex="http://tolex"
                exclude-result-prefixes="#all"
                version="3.0">

    <xsl:output method="xml"/>

    <xsl:param name="metadatafile" select="'/db/apps/teidictjson/data/onfw_metadata.xml'"/>

    <xsl:template match="tei:sourceDesc">
        <xsl:comment>
            Original:
            <xsl:value-of select="serialize(.)"/>
        </xsl:comment>
        <xsl:copy>
            <p>collection: <xsl:value-of select=".//tei:collection/@ref"/>, id: <xsl:value-of select=".//tei:idno"/></p>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:teiCorpus[tei:TEI]">
        <TEI>
            <xsl:apply-templates select="tei:teiHeader"/>
            <xsl:apply-templates select="tei:TEI"/>
        </TEI>
    </xsl:template>

    <xsl:template match="tei:teiCorpus[not(tei:TEI)]">
        <xsl:message terminate="yes">teiCorpus without TEI not supported</xsl:message>
    </xsl:template>

    <xsl:template match="tei:TEI[parent::tei:teiCorpus]">
        <xsl:comment>
            Original teiHeader
            <xsl:value-of select="serialize(tei:teiHeader)"/>
        </xsl:comment>
        <xsl:apply-templates select="tei:text"/>
    </xsl:template>

    <xsl:template match="tei:TEI[not(parent::tei:teiCorpus)]">
        <xsl:copy>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:publicationStmt">
        <xsl:copy>
            <xsl:choose>
                <xsl:when test="not(publisher or availability)">
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:when>
                <xsl:otherwise>
                        <xsl:copy-of select="doc( $metadatafile )//tei:publisher[1]"/>
                        <xsl:copy-of select="doc($metadatafile)//tei:availability[1]"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>

    <xsl:function name="tolex:genId">
        <xsl:param name="cur"/>
        <xsl:value-of select="replace(replace(encode-for-uri($cur/ancestor::tei:TEI/tei:teiHeader//tei:idno/text()),'%','_'),'^([0-9])','__$1')"/>
    </xsl:function>

    <xsl:template match="tei:superEntry">
        <entry>
            <xsl:attribute name="xml:id" select="tolex:genId(.)"/>
            <xsl:attribute name="xml:lang" select="tei:entry[1]/tei:form/@xml:lang"/>
            <xsl:for-each select="tei:entry">
                <xsl:copy>
                    <xsl:attribute name="xml:id" select="concat(tolex:genId(.),'_',position())"/>
                    <xsl:attribute name="xml:lang" select="tei:form/@xml:lang"/>
                    <xsl:apply-templates/>
                </xsl:copy>
            </xsl:for-each>
        </entry>
    </xsl:template>

    <xsl:template match="tei:entry">
        <xsl:copy>
            <xsl:attribute name="xml:id" select="tolex:genId(.)"/>
            <xsl:attribute name="xml:lang" select="tei:form/@xml:lang"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:sense">
        <xsl:copy>
            <xsl:attribute name="xml:id">
                <xsl:variable name="senseSeq">
                    <xsl:choose>
                        <xsl:when test="ancestor::tei:sense">
                            <xsl:value-of select="count(preceding::tei:sense[ancestor::tei:entry=current()/ancestor::tei:entry])+1+
                                count(ancestor::tei:sense)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="count(preceding::tei:sense[ancestor::tei:entry=current()/ancestor::tei:entry])+1"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="ancestor::tei:superEntry">
                        <xsl:variable name="pos" select="count(ancestor::tei:entry/preceding-sibling::tei:entry)+1"/>
                        <xsl:attribute name="xml:id" select="concat(tolex:genId(.),'_',$pos,'.',$senseSeq)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="xml:id" select="concat(tolex:genId(.),'.',$senseSeq)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:textDesc">
        <xsl:comment>
            Original:
            <xsl:value-of select="serialize(.)"/>
        </xsl:comment>
    </xsl:template>

    <xsl:template match="tei:msIdentifier">
        <xsl:comment>
            Original:
            <xsl:value-of select="serialize(.)"/>
        </xsl:comment>
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="tei:cit[@type='sensegroup']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:cit">
        <xsl:copy>
            <xsl:attribute name="type">
                <xsl:choose>
                    <xsl:when test="@type='proverb'">example</xsl:when>
                    <xsl:when test="@type='collocation'">example</xsl:when>
                    <xsl:otherwise><xsl:value-of select="@type"/> </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates select="@*[name()!='type']"/>
            <xsl:choose>
                <xsl:when test="@type='proverb'"><note type="textType">proverb</note></xsl:when>
                <xsl:when test="@type='collocation'"><note type="textType">collocation</note></xsl:when>
            </xsl:choose>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:cit/tei:def">
        <xsl:comment>was cit/def which isn't supported</xsl:comment>
        <note type="def">
            <xsl:apply-templates/>
        </note>
    </xsl:template>

    <xsl:template match="tei:etym">
        <!-- etym under quote and form not supported in lex0, turn into note -->
        <xsl:if test="parent::tei:form or parent::tei:def">
            <xsl:apply-templates select="tei:usg"/>
        </xsl:if>
        <note type="etym">
            <!-- TODO: original content under etym may not be supported in lex0 -->
            <xsl:apply-templates
                    select="if (parent::tei:form) then node[not(self::tei:usg)] else node()"/>
        </note>
    </xsl:template>

    <xsl:template match="tei:quote/tei:etym/tei:usg|tei:def/tei:etym/tei:usg">
        Usage <xsl:value-of select="@type"/>: <xsl:value-of select="."/>.
    </xsl:template>

    <xsl:template match="tei:q">
        <xsl:comment>start tei:q, not supported</xsl:comment>
        <xsl:apply-templates/>
        <xsl:comment>end tei:q</xsl:comment>
    </xsl:template>

    <xsl:template match="tei:article">
        <gram type="det"><xsl:value-of select="."/></gram>
    </xsl:template>

    <xsl:template match="tei:gram">
        <xsl:copy>
            <xsl:attribute name="type" select="substring-before(.,'.')"/>
            <xsl:value-of select="substring-after(.,'.')"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:ref">
        <xsl:copy>
            <xsl:attribute name="type" select="'general'"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:usg">
        <xsl:copy>
            <xsl:attribute name="type">
                <xsl:choose>
                    <xsl:when test="@type='geo'">geographic</xsl:when>
                    <xsl:when test="@type='freq'">frequency</xsl:when>
                    <xsl:when test="@type='lang'">hint</xsl:when>
                    <xsl:when test="@type='connotation'">meaningType</xsl:when>
                    <xsl:when test="@type='style'">textType</xsl:when>
                    <xsl:when test="@type='medium'">hint</xsl:when>
                    <xsl:otherwise><xsl:value-of select="@type"/></xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="@type='lang'"><note type="usg">lang</note></xsl:when>
                <xsl:when test="@type='style'"><note type="usg">style</note></xsl:when>
                <xsl:when test="@type='medium'"><note type="usg">medium</note></xsl:when>
            </xsl:choose>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:form">
        <xsl:copy>
            <xsl:apply-templates select="@*[not(name()='namekind')]"/>
            <xsl:if test="@namekind">
                <note type="namedentity"><xsl:value-of select="@namekind"/></note>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:msName|@namekind"/>

    <xsl:template match="tei:revisionDesc">
        <xsl:comment>
            Original:
            <xsl:value-of select="serialize(.)"/>
        </xsl:comment>
    </xsl:template>

    <xsl:template match="@*|node()" priority="-1">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
