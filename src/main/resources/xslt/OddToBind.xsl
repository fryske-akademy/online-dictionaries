<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  TeiLinguisticsFa
  %%
  Copyright (C) 2018 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">
	<xsl:output indent="yes"/>

    <xsl:template match="/">
        <jxb:bindings xmlns:jxb="https://jakarta.ee/xml/ns/jaxb"
                       xmlns:tei="http://www.tei-c.org/ns/1.0"
                       xmlns:xjc="http://java.sun.com/xml/ns/jaxb/xjc"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.0"
                       xsi:schemaLocation="https://jakarta.ee/xml/ns/jaxb https://jakarta.ee/xml/ns/jaxb/bindingschema_3_0.xsd">
            
            <xsl:comment><![CDATA[
                GENERATED FILE!!!: USE FOR EXAMPLE "xsltproc OddToBind.xsl ../xsd/tei_dictionaries.compiled > bind.xml"
            ]]></xsl:comment>

            <jxb:bindings schemaLocation="../xsd/tei_dictionaries.xsd" node="/xs:schema">

                <jxb:bindings node="/xs:schema/xs:attributeGroup/xs:attribute[@name = 'rendition']">
                    <jxb:property name="rendering"/>
                </jxb:bindings>

                <jxb:bindings
                        node="/xs:schema/xs:element[@name = 'revisionDesc']/xs:complexType/xs:choice/xs:element[@ref = 'tei:change']">
                    <jxb:property name="revisionDesc_change"/>
                </jxb:bindings>

                <jxb:bindings node="/xs:schema/xs:attributeGroup/xs:attribute[@name = 'value']">
                    <jxb:property name="value_unrealized"/>
                </jxb:bindings>

                <jxb:bindings
                        node="/xs:schema/xs:element[@name = 'model.divPart']/xs:complexType">
                    <jxb:class name="ModelDivPartType"/>
                </jxb:bindings>

                <jxb:bindings
                        node="/xs:schema/xs:complexType[@name='body']">
                    <jxb:class name="BodyType"/>
                </jxb:bindings>

                <xsl:for-each select="//tei:elementSpec[@ident='gram']">
                    <jxb:bindings node="/xs:schema/xs:element[@name='gram']/xs:complexType/xs:simpleContent/xs:restriction/xs:simpleType">
                        <jxb:typesafeEnumClass name="content">
                            <xsl:if test="tei:desc">
                                <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                            </xsl:if>
                            <xsl:for-each select=".//tei:valItem">
                                <xsl:variable name="name">
                                    <xsl:value-of select="substring-before(@ident,'.')"/>
                                    <xsl:text>_</xsl:text>
                                    <xsl:value-of select="substring-after(@ident,'.')"/>
                                </xsl:variable>
                                <jxb:typesafeEnumMember value="{@ident}" name="{$name}">
                                    <xsl:if test="tei:desc">
                                        <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                                    </xsl:if>
                                </jxb:typesafeEnumMember>
                            </xsl:for-each>
                        </jxb:typesafeEnumClass>
                    </jxb:bindings>
                </xsl:for-each>

                <xsl:for-each select="
                    //tei:classSpec[@ident='att.namekinds']//tei:attDef |
                    //tei:elementSpec[@ident='cit']//tei:attDef |
                    //tei:elementSpec[@ident='form']//tei:attDef |
                    //tei:elementSpec[@ident='purpose']//tei:attDef
                ">
                    <xsl:variable name="pref">
                        <xsl:choose>
                            <xsl:when test="ancestor::tei:classSpec"></xsl:when>
                            <xsl:otherwise><xsl:value-of select="ancestor::tei:elementSpec/@ident"/></xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="node">
                        <xsl:choose>
                            <xsl:when test="ancestor::tei:classSpec">
                                <xsl:text>/xs:schema/xs:attributeGroup/xs:attribute[@name='</xsl:text>
                                <xsl:value-of select="@ident"/>
                                <xsl:text>']/xs:simpleType</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>/xs:schema/xs:element[@name='</xsl:text>
                                <xsl:value-of select="$pref"/>
                                <xsl:text>']/xs:complexType//xs:attribute[@name='</xsl:text>
                                <xsl:value-of select="@ident"/>
                                <xsl:text>']/xs:simpleType</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <jxb:bindings
                        node="{$node}">
                        <jxb:typesafeEnumClass name="{$pref}_{@ident}">
                            <xsl:if test="tei:desc">
                                <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                            </xsl:if>
                            <xsl:for-each select=".//tei:valItem">
                                <jxb:typesafeEnumMember value="{@ident}">
                                    <xsl:attribute name="name">
                                        <xsl:choose>
                                            <xsl:when test="number(@ident) = number(@ident)">
                                                <xsl:value-of select="concat('_',@ident)"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="@ident"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                    <xsl:if test="tei:desc">
                                        <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                                    </xsl:if>
                                </jxb:typesafeEnumMember>
                            </xsl:for-each>
                        </jxb:typesafeEnumClass>
                    </jxb:bindings>
                </xsl:for-each>
            </jxb:bindings>

            <jxb:globalBindings>
                <xjc:simple/>
            </jxb:globalBindings>

        </jxb:bindings>
    </xsl:template>

</xsl:stylesheet>
