<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>ISO Schematron rules</title>
   <!-- This file generated 2024-10-10T06:33:12Z by 'extract-isosch.xsl'. -->
   <!-- ********************* -->
   <!-- namespaces, declared: -->
   <!-- ********************* -->
   <ns prefix="tei" uri="http://www.tei-c.org/ns/1.0"/>
   <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
   <ns prefix="rng" uri="http://relaxng.org/ns/structure/1.0"/>
   <!-- ********************* -->
   <!-- namespaces, implicit: -->
   <!-- ********************* -->
   <ns prefix="dcr" uri="http://www.isocat.org/ns/dcr"/>
   <!-- ************ -->
   <!-- constraints: -->
   <!-- ************ -->
   <pattern id="schematron-constraint-tei_dictionaries-att.datable.w3c-att-datable-w3c-when-1">
      <rule context="tei:*[@when]">
         <report test="@notBefore|@notAfter|@from|@to" role="nonfatal">The @when attribute cannot be used with any other att.datable.w3c attributes.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-att.datable.w3c-att-datable-w3c-from-2">
      <rule context="tei:*[@from]">
         <report test="@notBefore" role="nonfatal">The @from and @notBefore attributes cannot be used together.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-att.datable.w3c-att-datable-w3c-to-3">
      <rule context="tei:*[@to]">
         <report test="@notAfter" role="nonfatal">The @to and @notAfter attributes cannot be used together.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-att.datable-calendar-calendar-4">
      <rule context="tei:*[@calendar]">
         <assert test="string-length(.) gt 0"> @calendar indicates one or more systems or calendars to
              which the date represented by the content of this element belongs, but this
              <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-att.global.source-source-only_1_ODD_source-5">
      <rule context="tei:*/@source">
         <let name="srcs" value="tokenize( normalize-space(.),' ')"/>
         <report test="( parent::tei:classRef                               | parent::tei:dataRef                               | parent::tei:elementRef                               | parent::tei:macroRef                               | parent::tei:moduleRef                               | parent::tei:schemaSpec )                               and                               $srcs[2]">
              When used on a schema description element (like
              <value-of select="name(..)"/>), the @source attribute
              should have only 1 value. (This one has <value-of select="count($srcs)"/>.)
            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-att.typed-subtypeTyped-6">
      <rule context="tei:*[@subtype]">
         <assert test="@type">The <name/> element should not be categorized in detail with @subtype unless also categorized in general with @type</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-att.pointing-targetLang-targetLang-7">
      <rule context="tei:*[not(self::tei:schemaSpec)][@targetLang]">
         <assert test="@target">@targetLang should only be used on <name/> if @target is specified.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-att.styleDef-schemeVersion-schemeVersionRequiresScheme-8">
      <rule context="tei:*[@schemeVersion]">
         <assert test="@scheme and not(@scheme = 'free')">
              @schemeVersion can only be used if @scheme is specified.
            </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-p-abstractModel-structure-p-in-ab-or-p-9">
      <rule context="tei:p">
         <report test="    (ancestor::tei:ab or ancestor::tei:p)                          and not( ancestor::tei:floatingText                                 |parent::tei:exemplum                                 |parent::tei:item                                 |parent::tei:note                                 |parent::tei:q                                 |parent::tei:quote                                 |parent::tei:remarks                                 |parent::tei:said                                 |parent::tei:sp                                 |parent::tei:stage                                 |parent::tei:cell                                 |parent::tei:figure                                )">
        Abstract model violation: Paragraphs may not occur inside other paragraphs or ab elements.
      </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-p-abstractModel-structure-p-in-l-or-lg-10">
      <rule context="tei:p">
         <report test="    (ancestor::tei:l or ancestor::tei:lg)                          and not( ancestor::tei:floatingText                                 |parent::tei:figure                                 |parent::tei:note                                )">
        Abstract model violation: Lines may not contain higher-level structural elements such as div, p, or ab, unless p is a child of figure or note, or is a descendant of floatingText.
      </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-cit-cit_check-11">
      <rule context="tei:cit">
         <report test="(@type='sensegroup' and not(tei:sense)) or                 (tei:form and @type!='translation') or                 ((@type='example' or @type='collocation' or @type='proverb') and not(tei:quote))">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : @type=sensegroup requires cit/sense, @type!=translation|sensegroup requires cit/quote,
                                form requires @type=translation
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-cit-sensegroup_check-12">
      <rule context="tei:cit">
         <report test="@type='sensegroup' and *[not(self::tei:note or self::tei:sense)]">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : @type=sensegroup may only contain sense and note
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-cit-quote_check-13">
      <rule context="tei:cit">
         <report test="count(tei:quote) &gt; 1 and @type!='translation'">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : multiple quotes requires @type=translation
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-cit-def_check-14">
      <rule context="tei:cit">
         <report test="tei:def and @type!='collocation' and @type!='proverb'">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : def is only for collocation and proverb
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-cit-translationlang_check-15">
      <rule context="tei:cit">
         <report test="@type='translation' and not(@xml:lang)">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : translation requires @xml:lang
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-cit-citdepth_check-16">
      <rule context="tei:cit">
         <report test="tei:cit/tei:cit/tei:cit">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : maximum nesting depth for cit is 2
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-cit-nestedcittype_check-17">
      <rule context="tei:cit">
         <report test="ancestor::tei:cit and @type!='translation' and @type!='example' and @type!='sensegroup'">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : nested cit must be translation, example or sensegroup
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-ref-refAtts-18">
      <rule context="tei:ref">
         <report test="@target and @cRef">Only one of the
	attributes @target' and @cRef' may be supplied on <name/>
         </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-list-gloss-list-must-have-labels-19">
      <rule context="tei:list[@type='gloss']">
         <assert test="tei:label">The content of a "gloss" list should include a sequence of one or more pairs of a label element followed by an item element</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-entry-form_check-20">
      <rule context="tei:entry">
         <assert test="tei:form/@type='lemma'">
            <value-of select="tei:form/tei:orth"/>: entry/form/@type must be of type "lemma"
                            </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-entry-translation_check-21">
      <rule context="tei:entry">
         <report test="tei:cit[@type='translation']/tei:quote or tei:sense/tei:cit[@type='translation']/tei:quote">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : cit/@type='translation requires form, not quote'
                                <value-of select="ancestor::TEI/teiHeader/fileDesc/titleStmt/title"/>
         </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-entry-citform_check-22">
      <rule context="tei:entry">
         <report test="tei:cit[@type!='translation']/tei:form">
            <value-of select="tei:cit[@type!='translation']/tei:form/tei:orth"/>: cit/form requires
                                cit/@type='translation'
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-entry-citform_check2-23">
      <rule context="tei:entry">
         <report test="tei:sense/tei:cit[@type!='translation']/tei:form">
            <value-of select="tei:sense/tei:cit[@type!='translation']/tei:form/tei:orth"/>: cit/form requires
                                cit/@type='translation'
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-form-namekind_check-24">
      <rule context="tei:form">
         <report test="@namekind and not(@type='lemma')">
            <value-of select="tei:orth"/>: define @namekind on a lemma form
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-form-lemma_check-25">
      <rule context="tei:form">
         <report test="@type='lemma' and parent::tei:form">
            <value-of select="tei:orth"/>: A lemma form must be at the highest level of a form tree
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-form-lemmadepth_check-26">
      <rule context="tei:form">
         <report test="tei:form/tei:form/tei:form">
            <value-of select="tei:orth"/>: maximum 2 levels of form nesting allowed
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-def-defempty_check-27">
      <rule context="tei:def">
         <report test="not(node())">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : def should contain something
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-textDesc-purpose_check-28">
      <rule context="tei:textDesc">
         <assert test="ancestor::tei:teiHeader[parent::tei:teiCorpus]">
            <value-of select="ancestor::tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno"/>
                                : textDesc/purpose should be in teiCorpus/teiHeader
                                not in TEI/teiHeader
                            </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-quotation-quotationContents-29">
      <rule context="tei:quotation">
         <report test="not(@marks) and not (tei:p)">
On <name/>, either the @marks attribute should be used, or a paragraph of description provided</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-citeStructure-delim-citestructure-inner-delim-30">
      <rule context="tei:citeStructure[parent::tei:citeStructure]">
         <assert test="@delim">A <name/> with a parent <name/> must have a @delim attribute.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-citeStructure-match-citestructure-outer-match-31">
      <rule context="tei:citeStructure[not(parent::tei:citeStructure)]">
         <assert test="starts-with(@match,'/')">An XPath in @match on the outer <name/> must start with '/'.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-citeStructure-match-citestructure-inner-match-32">
      <rule context="tei:citeStructure[parent::tei:citeStructure]">
         <assert test="not(starts-with(@match,'/'))">An XPath in @match must not start with '/' except on the outer <name/>.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-tei_dictionaries-TEI-idno-check-33">
      <rule context="tei:TEI">
         <assert test="tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno">
            <value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>: missing
                                teiHeader/fileDesc/sourceDesc/msDesc/msIdentifier/idno.
                            </assert>
      </rule>
   </pattern>
</schema>
