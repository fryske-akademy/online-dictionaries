/*-
 * #%L
 * teidictionaries
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import name.dmaus.schxslt.SchematronException;
import org.fryske_akademy.teidictionaries.jaxb.Gram;
import org.fryske_akademy.transformation.XslTransformer;
import org.fryske_akademy.validation.FA_RngValidationHelper;
import org.fryske_akademy.validation.FA_ValidationHelper;
import org.fryske_akademy.teidictionaries.jaxb.TeiCorpus;
import org.xml.sax.SAXException;

import jakarta.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Files;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestValidation {

    @Test
    public void testValidation() {
        try {
            FA_ValidationHelper.validateXml(new URL("file:src/test/resources/'kfout.xml"));
            try {
                FA_RngValidationHelper.validateRngSchematron(new URL("file:src/test/resources/'kfout.xml"));
                Assertions.fail("schematron should fail");
            } catch (SchematronException ex) {
                System.out.println(ex.getMessage());
            }
            FA_RngValidationHelper.validateRngSchematron(new URL("file:src/test/resources/'kgoed.xml"));
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
    public void testTextValidation() {
        try {
            FA_ValidationHelper.validateXml(new URL("file:src/test/resources/formtextfout.xml"));
            Assertions.fail("text under form shouldn't be allowed");
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void testLex0() throws TransformerException, IOException, SchematronException, SAXException {
        XslTransformer.FaToLex0(new FileReader("src/test/resources/'kgoed.xml"), new StringWriter(), true);
        XslTransformer.FaToLex0(new FileReader("src/test/resources/'kLex0.xml"), new StringWriter(), true);
    }

    @Test
    public void testXml() throws IOException, SAXException, SchematronException, JAXBException {
        byte[] xml = Files.readAllBytes(new File("src/test/resources/'kgoed.xml").toPath());
        FA_ValidationHelper.validateXml(new String(xml));
        TeiCorpus fromXML = FA_ValidationHelper.fromXML(new StringReader(new String(xml)),TeiCorpus.class);
        String copy = FA_ValidationHelper.toXML(fromXML);
        FA_RngValidationHelper.validateRngSchematron(copy);

        Gram gram = new Gram();
        gram.setValue(Gram.Content.abbr_yes);
    }
}
