xquery version "3.1";

module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";

import module namespace templates="http://exist-db.org/xquery/html-templating";
import module namespace xmldb="http://exist-db.org/xquery/xmldb";
import module namespace request="http://exist-db.org/xquery/request";
import module namespace response="http://exist-db.org/xquery/response";
import module namespace ft="http://exist-db.org/xquery/lucene";
import module namespace util="http://exist-db.org/xquery/util";
import module namespace console = "http://exist-db.org/xquery/console";
import module namespace cache = "http://exist-db.org/xquery/cache";
import module namespace transform="http://exist-db.org/xquery/transform";

declare namespace properties="http://exist-db.org/xquery/properties";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace map="http://www.w3.org/2005/xpath-functions/map";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace fn="http://www.w3.org/2005/xpath-functions";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace err = "http://www.w3.org/2005/xqt-errors";

declare variable $teidictjson:props := properties:loadProperties("teidictjson.properties");

declare function teidictjson:getProperty($key as xs:string) as xs:string {
    teidictjson:getProperty($key,"")
};

declare function teidictjson:getProperty($key as xs:string, $default as xs:string) as xs:string {
    if (map:contains($teidictjson:props,$key)) then
        map:get($teidictjson:props,$key)
    else
        $default
};

(: The properties below determine the behaviour of the application, change them in teidict.properties as needed:)
declare variable $teidictjson:fromlang := teidictjson:getProperty("fromlang","nl");
declare variable $teidictjson:tolang := teidictjson:getProperty("tolang","fry");
declare variable $teidictjson:maxhits := xs:integer(teidictjson:getProperty("maxHits","10"));
declare variable $teidictjson:cache := xs:boolean(teidictjson:getProperty("cache","false"));
declare variable $teidictjson:sortresults := xs:boolean(teidictjson:getProperty("sortresults","false"));

(: constants :)
declare variable $teidictjson:pos := ("noun","adj","adp","adv","verb","aux","num","pron","propn","cconj","sconj","intj","det","x");
declare variable $teidictjson:sections := ("alltext","lemmas","example","collocation","proverb");
declare variable $teidictjson:capacities := (
                "formtranslation",
                "searchText",
                "synonyms",
                "variants",
                "compounds",
                "pronunciation",
                "hyphenation",
                "grammar",
                "paradigm",
                "examples",
                "collocations",
                "proverbs",
                "usage",
                "stress",
                "definition"
);
declare variable $teidictjson:usg := (
                                "lang",
                                "time",
                                "freq",
                                "connotation",
                                "hint",
                                "style",
                                "geo",
                                "medium",
                                "domain"
);

declare variable $teidictjson:cr := cache:create("teidictjson",map{});

declare variable $teidictjson:parallelxslt := doc($config:app-root||"/teiToParallel.xslt");
declare variable $teidictjson:rrxslt := doc($config:app-root||"/readableResults.xslt");
declare variable $teidictjson:rdxslt := doc($config:app-root||"/readableDictXml.xslt");
declare variable $teidictjson:notextxslt := doc($config:app-root||"/noText.xslt");
declare variable $teidictjson:jsonxslt := doc($config:app-root||"/tojson.xslt");
declare variable $teidictjson:lex0xslt := doc($config:app-root||"/teiToLex0.xslt");
declare variable $teidictjson:jsonparams := <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
  <output:method value="json"/>
  <exist:json-ignore-whitespace-text-nodes value="yes"/>
</output:serialization-parameters>;
declare variable $teidictjson:luceneOpts := map {
    "default-operator" : "or",
    "leading-wildcard": "yes",
    "filter-rewrite": "yes"
};

declare variable $teidictjson:data := collection($config:data-root);

(: List containing lemmas that are considered complex :)
declare variable $teidictjson:complexList :=
    if (fn:not(fn:empty(cache:get("teidictjson","complexList")))) then cache:get("teidictjson","complexList")
    else (let $r := cache:put("teidictjson","complexList",
        if (fn:unparsed-text-available($config:data-root||"/complex.txt")) then fn:unparsed-text-lines($config:data-root||"/complex.txt") else ())
    return cache:get("teidictjson","complexList")
    );

declare variable $teidictjson:collectionid := head($teidictjson:data/tei:teiCorpus//tei:idno);

declare function teidictjson:browserCache($c) {
        if (fn:not($c)) then response:set-header('Cache-Control','no-cache') else ()
};
declare function teidictjson:browserCache() {
        teidictjson:browserCache($teidictjson:cache)
};
(:~
 :   function to autocomplete user input by looking for tei:orth that start with it.
 :   Required parameter q the string to autocomplete, restricted to "^[a-zA-Z-]*$"
 :   Optional parameters: searchlang to determine search language, pos to restrict to part of speech
:)
declare function teidictjson:autocomplete($q as xs:string, $searchlang as xs:string?, $pos as xs:string?) as xs:string {
    (teidictjson:json(),teidictjson:browserCache(),fn:serialize(try {
  if ($q and fn:matches($q,"^[0-9a-zA-Z-]*$")) then
      let $l := if ($searchlang=$teidictjson:tolang) then $teidictjson:tolang else $teidictjson:fromlang
      let $results := fn:sort(
             if ($pos) then
              $teidictjson:data//tei:orth[ft:query(.,$q || "*", teidictjson:luceneopts())]
                  [ancestor::tei:form[@xml:lang=$l][tei:gram[.='pos.'||$pos]]]/text()
             else
              $teidictjson:data//tei:orth[ft:query(.,$q || "*", teidictjson:luceneopts())][ancestor::tei:form[@xml:lang=$l]]/text()
           )
      return
          if ($results) then array {
          for $item in fn:distinct-values(fn:subsequence($results,1,30))
          return if (fn:contains($item," ")) then '"' || $item || '"' else $item}
      else () else ()
  } catch * {
      ()
  },$teidictjson:jsonparams))
};
(:~
 :   function check if there is a tei:orth.
 : Obeys searchlang parameter
:)
declare function teidictjson:contains($form as xs:string?) as xs:string {
    teidictjson:json(),teidictjson:browserCache(),
    if ($form) then "["||fn:exists(
            $teidictjson:data//tei:orth[ft:query(.,$form,
            teidictjson:facets(not(teidictjson:isFromLang()),teidictjson:luceneopts()))]
            )||"]"
    else "[false]"
};

(:~
 :   function check if text is present
 : Obeys searchlang parameter
:)
declare function teidictjson:containsText($form as xs:string?) as xs:string {
    teidictjson:json(),teidictjson:browserCache(),
    if ($form) then
        "["||fn:exists(
                    $teidictjson:data//tei:quote[ft:query(.,teidictjson:searchTerm($form),
                    teidictjson:facets(not(teidictjson:isFromLang()),teidictjson:luceneopts()))]
        )||"]"
    else "[false]"
};

(:~
: a title for the dictionary
:)
declare
%templates:wrap
function teidictjson:title($node as node(), $model as map(*)) as xs:string {
    if ($teidictjson:data/tei:teiCorpus//tei:title[@xml:lang="en"]) then
        $teidictjson:data/tei:teiCorpus//tei:title[@xml:lang="en"][1]/text()
    else
        if ($teidictjson:data/tei:teiCorpus//tei:title[1]/text()) then
            $teidictjson:data/tei:teiCorpus//tei:title[1]/text()
        else "missing teiCorpus"
};

(:~ lookup the tei:idno of this dictionary
:)
declare function teidictjson:dictId() as xs:string {
    $teidictjson:data/tei:teiCorpus/tei:teiHeader//tei:idno
};

(:~ lookup the tei:idno (lemma) in the xml for the node argument.
    @return the id (=lemma) as string
:)
declare function teidictjson:docId($node as node()) as xs:string {
    head(root($node)//tei:teiHeader//tei:idno)
};

(:~ lookup the tei:idno (lemma) in the id index field.
    @return the id (=lemma) as string
:)
declare function teidictjson:docIdF($node as node()) as xs:string {
    ft:field($node,"id")
};

(:~
:    determines the UD pos (i.e. pos.noun) for a node
:    @param $node the for which we seek pos
:)
declare function teidictjson:pos($node as node()?) as xs:string {
    let $posfield := ft:field($node,"pos")
    let $p := if ($posfield) then $posfield else head($node/ancestor-or-self::tei:entry//tei:gram[fn:starts-with(.,'pos.')]/text())
    return if ($p) then $p else "pos.x"
};

(:~
: function to present text under tei:def (definitions) as html
:)
declare function teidictjson:definitions($def as node()*) as node()* {
    let $entrypos := fn:count($def/ancestor::tei:entry/preceding::tei:entry) + 1
    for $d in $def
    return transform:transform($d,$teidictjson:rdxslt,
        <parameters>
            <param name="inline" value="{fn:true()}"/>
            <param name="entrypos" value="{$entrypos}"/>
        </parameters>
     )
};

declare function teidictjson:highlight($toHighlight as node(), $searchterm as xs:string) as node() {
    if (teidictjson:noHighlight()) then $toHighlight else
        if (fn:starts-with($searchterm,"sensitive:(")) then ft:highlight-field-matches($toHighlight,"sensitive") else util:expand($toHighlight)
};

(: find lemmas that refer to the argument lemma by looking for ^lemma.xml in @target or content that equals lemma :)
declare function teidictjson:referers($lemma as xs:string) as node()* {
   teidictjson:uniqueRefs(($teidictjson:data//tei:ref
   [fn:starts-with(@target,$lemma||".xml")]|
   $teidictjson:data//tei:ref[.=$lemma]),map{})
};

(:~
    returns only the first of the refs with the same text and @target
:)
declare function teidictjson:uniqueRefs($refs as node()*, $unique as map(xs:string,element(tei:ref))) {
    if (fn:empty($refs)) then map:for-each($unique,function($k,$v) {$v}) else
        let $r := fn:subsequence($refs,1,1)
        let $k := $r/text()||$r/@target
        return teidictjson:uniqueRefs(
            if (fn:count($refs)>1) then fn:subsequence($refs,2) else (),
            if ($k) then
            if (map:contains($unique,$k)) then $unique else map:put($unique,$k,$r)
            else $unique
        )
};

(:~
:    return lemmas that refer to the argument lemma as strings by looking for the lemma in @target
:)
declare function teidictjson:refererStrings($lemma as xs:string) as xs:string* {
   fn:distinct-values(for $ref in teidictjson:referers($lemma)
    let $refid := teidictjson:docId($ref)
    where $refid!=$lemma
    order by $refid
   return if (fn:head($ref/ancestor::*[@xml:id])) then
            $refid || ".xml#" || fn:head(fn:reverse($ref/ancestor::*[@xml:id]))/@xml:id
          else
            $refid || ".xml"
    )
};

(:~
: retrieves lemma xml from collection
:)
declare function teidictjson:xmlSource($lemma as xs:string, $readable as xs:boolean, $notext as xs:boolean) as item()? {
    (   teidictjson:browserCache(),
        let $tei := if ($notext) then
            transform:transform(teidictjson:source($lemma),$teidictjson:notextxslt,())
        else
            teidictjson:source($lemma)
        return if ($lemma and $readable!=fn:true()) then $tei
            else if ($lemma and $readable=fn:true())
            then transform:stream-transform($tei,$teidictjson:rdxslt,())
        else <notFound>{$lemma}</notFound>
    )
};

(:~
: retrieves lemma xml from collection
:)
declare function teidictjson:source($lemma as xs:string) as node()? {
    let $t := $teidictjson:data//tei:idno[ft:query(.,'"' || $lemma || '"')]
    return if (count($t) > 1) then
    let $u := util:log("warn","found multiple sources (returning first !pos.x): " ||
            fn:string-join( for $s in $t return util:document-name($s) ,", "))
        return head(for $s in $t where teidictjson:pos($s/ancestor::tei:TEI//tei:entry[1])!="pos.x" return $s/ancestor::tei:TEI)
     else root($t)/tei:TEI
};


(:~
:    function to retrieve details in json
:    @param $lemma the name of the lemma to link to
:)
declare function teidictjson:detailsJSON($lemma as xs:string) {
    let $tei := teidictjson:source($lemma)
    let $search := request:get-parameter('searchterm','')
    return (
        response:set-header('Content-Type', 'application/json; charset=utf-8'),
        response:set-header('Content-Disposition', 'inline; filename=details.json'),
        teidictjson:browserCache(),
        if ($tei) then
            let $re := <tei:superEntry xmlSource="xmlSource?lemma={$lemma}"
                            collection="{$teidictjson:collectionid}">{
                for $entry in $tei//tei:entry
                return $entry,
                for $s in teidictjson:refererStrings($lemma) return <referer>{$s}</referer>
            }</tei:superEntry>
            return
        fn:serialize(
            transform:transform($re,$teidictjson:rdxslt,()),$teidictjson:jsonparams)
        else
            fn:serialize(<noresults>no results</noresults>,$teidictjson:jsonparams)
    )
};

declare function teidictjson:luceneopts() {
    $teidictjson:luceneOpts
};

declare function teidictjson:facets($tr as xs:boolean, $pos as xs:string, $opts as map(*)) {
    map:merge(($opts,map {
                          "facets": map {
                              "translation": $tr,
                              "pos": $pos
                          },
                          "fields": ("lemma", "pos")
                      }))
};

declare function teidictjson:facets($tr as xs:boolean, $opts as map(*)) {
    map:merge(($opts,map {
                          "facets": map {
                              "translation": $tr
                          },
                          "fields": ("lemma", "pos")
                      }))
};

declare function teidictjson:searchTerm($term as xs:string) as xs:string {
    let $se := request:get-parameter("sensitive","")
    return if ($se = "") then
    $term else if ($se = "false") then $term else "sensitive:(" || $term || ")"
};

declare function teidictjson:browsing() {
    let $term := fn:normalize-space(request:get-parameter("searchterm",""))
    return $term = "" or $term = '*'
};

(:~
    bottleneck function to look for words. Does not sort results, instead you can use ft:field($o,"lemma") to sort if you want to.
:)
declare function teidictjson:queryWords($searchlang as xs:string?, $searchterm as xs:string?) as element(tei:orth)* {
       let $sl := if ($searchlang) then $searchlang else $teidictjson:fromlang
       let $pos := request:get-parameter('pos','')
       let $browse := teidictjson:browsing()
       return
           if ($browse) then
                if ($pos) then
                $teidictjson:data//tei:form[@xml:lang=$sl]
                    [descendant::tei:gram[.='pos.'||$pos]]
                /tei:orth[ft:query(., "*",
                teidictjson:facets($sl=$teidictjson:tolang,teidictjson:luceneopts()))]
                else
                $teidictjson:data//tei:form[@xml:lang=$sl]/tei:orth[ft:query(., "*",
               teidictjson:facets($sl=$teidictjson:tolang,teidictjson:luceneopts()))]
           else
                if ($pos) then
                    $teidictjson:data//tei:orth
                        [ft:query(., teidictjson:searchTerm($searchterm),
                        teidictjson:facets($sl=$teidictjson:tolang,
                        'pos.'||$pos,
                        teidictjson:luceneopts()))]
               else
                    $teidictjson:data//tei:orth
                        [ft:query(., teidictjson:searchTerm($searchterm),
                        teidictjson:facets($sl=$teidictjson:tolang,teidictjson:luceneopts()))]
};


(:~
:    returns $teidictjson:fromlang or request parameter searchlang
:)
declare function teidictjson:getLang() as xs:string {
    request:get-parameter("searchlang",$teidictjson:fromlang)
};

(:~
:    returns true when teidictjson:getLang() equals $teidictjson:fromlang
:)
declare function teidictjson:isFromLang() as xs:boolean {
    teidictjson:getLang()=$teidictjson:fromlang
};

(:~
:    returns true when no expensive highlighting should be applied
:)
declare function teidictjson:noHighlight() as xs:boolean {
    $teidictjson:nohighlight
};

declare variable $teidictjson:nohighlight :=
    fn:not(xs:boolean(request:get-parameter("highlight","false"))) or
    teidictjson:browsing() or
    "lemmas" = request:get-parameter('section','');

(:~
:
:    function to search in text returning json.
:    @param $searchlang the language to search in, see teidictjson:fromlang and teidictjson:tolang
:    @param $searchterm the (lucene) searchterm
:    @param $offset from where do we present results to the user
:    @param $section the text category to search in, see $teidictjson:textcategories
:)
declare function teidictjson:searchJSON($searchlang as xs:string?, $searchterm as xs:string?, $offset as xs:integer, $section as xs:string?) {
    (teidictjson:json($section),
    teidictjson:browserCache(),
    transform:stream-transform(transform:transform(
            try {
                teidictjson:search($searchterm, $offset, $section)
            } catch * {
                (util:log("debug", $err:code || ": " || $err:description || $err:line-number || ':' || $err:column-number),
                    <tei:hi>query too complex</tei:hi>)
            }
        ,
            $teidictjson:rrxslt,()),$teidictjson:jsonxslt,())
    )
};

declare function teidictjson:formsWithTranslation($node as node(), $model as map(*)) {
    (response:set-header('Content-Type', 'application/json; charset=utf-8'),
        response:set-header('Content-Disposition', 'inline; filename=lemmas.json'),
    teidictjson:browserCache(),
    '{"lemma":[',
    for $f at $p in $teidictjson:data//tei:entry/tei:form
    let $trs := $f/..//tei:cit[@type='translation']/tei:form/tei:orth/text()
    let $tr := if (fn:count($trs)>1) then fn:distinct-values($trs) else $trs
    return (
    if ($p>1) then "," else "",
    '{"#text":"',
        teidictjson:sanitizeJson($f/tei:orth/text()),
        '",',
        '"translation":[',
        for $t at $pp in $tr
        return (
            if ($pp>1) then "," else "",
            '"',teidictjson:sanitizeJson($t),'"'

        ),
    ']}'
    ),
    
    ']}'
)};

declare function teidictjson:sanitizeJson($s as xs:string) as xs:string {
    if (fn:contains($s,'"')) then
        fn:replace(fn:normalize-unicode(fn:normalize-space($s)),'"','\\"')
    else
        fn:normalize-unicode(fn:normalize-space($s))
};
declare function teidictjson:parallelText($replaceBrackets as xs:boolean) {
    teidictjson:text(),
    transform:stream-transform((),$teidictjson:parallelxslt,
        <parameters>
            <param name="dir" value="xmldb:exist://{$config:data-root}"/>
            <param name="replaceBrackets" value="{$replaceBrackets}"/>
        </parameters>)
};
declare function teidictjson:lex0($lemma as xs:string) {
    transform:stream-transform(teidictjson:source($lemma),$teidictjson:lex0xslt,
        <parameters>
            <param name="metadatafile" value="{$config:data-root || '/' || fn:lower-case(teidictjson:dictId()) || '_metadata.xml'}"/>
        </parameters>)
};

declare function teidictjson:textsWithTranslations($node as node(), $model as map(*)) {
    (teidictjson:text(),
    teidictjson:browserCache(),
    for $q in $teidictjson:data//tei:cit[@type!="translation"]/tei:quote
    return (
        teidictjson:quoteText($q),'&#10;',
        for $t in $q/following-sibling::tei:cit[@type="translation"]/tei:quote
        return ('tr: '||teidictjson:quoteText($t), '&#10;'),
        '&#10;'
    )
)};

declare function teidictjson:quoteText($q as element(tei:quote)) as xs:string {
    fn:normalize-space(fn:replace(fn:string-join($q//text()[fn:not(ancestor::tei:etym or ancestor::tei:note or ancestor::tei:bibl)],""),'[&#13;&#10;];',''))
};

declare function teidictjson:query($section as xs:string, $searchterm as xs:string?) as node()* {
   let $browse := teidictjson:browsing()
   let $l := teidictjson:getLang()
   return
   (if ($section='alltext') then ((: search in lemmas and text :)
        teidictjson:queryWords($l,$searchterm),
        if ($browse) then
           if ($l=$teidictjson:fromlang) then
                $teidictjson:data//tei:cit[@type!='translation']/tei:quote[ft:query(.,())]
            else
                $teidictjson:data//tei:cit[@type='translation']/tei:quote[ft:query(.,())]
        else
           if ($l=$teidictjson:fromlang) then
                $teidictjson:data//tei:quote
                    [ft:query(., teidictjson:searchTerm($searchterm), teidictjson:luceneopts())]
                    [parent::tei:cit[@type!='translation']]
            else
                $teidictjson:data//tei:quote
                    [ft:query(., teidictjson:searchTerm($searchterm), teidictjson:luceneopts())]
                    [parent::tei:cit[@type='translation']]
    ) else if ($section='lemmas') then teidictjson:queryWords($l,$searchterm) else (: search only in lemmas :)
       if ($browse) then (: search only in a specific section :)
           if ($l=$teidictjson:fromlang) then
               $teidictjson:data//tei:cit[@type=$section]/tei:quote[ft:query(.,())]
            else
               $teidictjson:data//tei:cit[@type=$section]/tei:cit[@type='translation']/tei:quote[ft:query(.,())]
        else
           if ($l=$teidictjson:fromlang) then
               $teidictjson:data//tei:cit[@type=$section]/tei:quote
                    [ft:query(., teidictjson:searchTerm($searchterm),
                     teidictjson:facets(fn:false(),teidictjson:luceneopts()))]
            else
               $teidictjson:data//tei:cit[@type=$section]//tei:quote
                    [ft:query(., teidictjson:searchTerm($searchterm),
                     teidictjson:facets(fn:true(),teidictjson:luceneopts()))]
    )
};

declare function teidictjson:findType($hit as node()) as xs:string {
    if ($hit instance of element(tei:orth)) then 'grammar' else
        if ($hit/ancestor::tei:cit[@type='example']) then 'example' else
            if ($hit/ancestor::tei:cit[@type='proverb']) then 'proverb' else
                if ($hit/ancestor::tei:cit[@type='collocation']) then 'collocation' else
                    $hit/../@type
};

(:~
:    bottleneck function to search in text, return xml in this form (for text results):
:<pre>
{
  "results": {
    "@total": 39514,
    "@max": 10,
    "@offset": 1,
    "@collection": "FIWB",
    "@querystring": "section=example",
    "result": [
      {
        "@lemma": "iepenhâlde",
        "@pos": "x",
        "@section": "example",
        "@parent": null,
        "@id": null,
        "text": [
          {
            "$": " De "
          },
          {
            "strong": {
              "$": "doar "
            }
          },
          {
            "$": " foar immen iepenhâlde "
          }
        ],
        "translation": {
          "@lang": "en",
          "$": "keep the door (open) for s.o."
        }
      },
:</pre>
:    @param $searchterm the (lucene) searchterm
:    @param $offset from where do we present results to the user
:    @param $section the text category to search in, see $teidictjson:textcategories
:)
declare function teidictjson:search($searchterm as xs:string?, $offset as xs:integer, $section as xs:string) as node() {
       let $browse := teidictjson:browsing()
       let $search := if ($searchterm) then $searchterm else ""
           let $hits := if ($teidictjson:sortresults) then
                            fn:sort(teidictjson:query($section,$search),(),
                                function($o) {head($o//text())})
                       else teidictjson:query($section,$search)
               return
            <results total="{fn:count($hits)}" max="{$teidictjson:maxhits}" offset="{$offset}"
                collection="{$teidictjson:collectionid}" querystring="{request:get-query-string()}">{
                for $hit in fn:subsequence($hits,xs:integer($offset),$teidictjson:maxhits)
                    let $lem := teidictjson:docIdF($hit)
                    let $id := fn:head( fn:reverse($hit/ancestor-or-self::*[@xml:id]))/@xml:id
                    let $t := teidictjson:findType($hit)
                    let $parent := if ($t = "example") then
                        $hit/ancestor::tei:cit[@type="collocation" or @type="proverb"]/@type else ""
                return <result lemma="{$lem}"
                    pos="{fn:substring-after(teidictjson:pos($hit),'.')}"
                    section="{$t}" parent="{$parent}" id="{$id}">{
                    if ($t="grammar") then (
                            $hit/parent::tei:form,
                            <translations>{teidictjson:translateForm($hit, teidictjson:isFromLang(),fn:false())}</translations>
                            )
                    else (
                        if ($browse) then $hit else teidictjson:highlight($hit,$search),
                            teidictjson:translateQuote(if (teidictjson:isFromLang()) then $hit else $hit/../../tei:quote)
                        )
                }</result>
            }
            </results>
};


(:~
:    determines if two forms have the same gram elements
:    @param $form1
:    @param $form2
:    @param $type for example "pos", only compare this grammar type
:)
declare function teidictjson:sameGram($form1 as node(), $form2 as node(), $type as xs:string?) as xs:boolean {
    if ($form1 and $form2) then
        if ($type) then
            $form1//tei:gram[fn:starts-with(.,$type||'.')] = $form2//tei:gram[fn:starts-with(.,$type||'.')]
        else
            fn:deep-equal(fn:sort($form1//tei:gram),fn:sort($form2//tei:gram))
    else fn:false()
};

(:~ return translations of the lemma for the given form:)
declare function teidictjson:translateForm($orth as element(tei:orth), $fromlang as xs:boolean, $onlyOrth as xs:boolean) as node()* {
    for $form in if ($fromlang) then
            $orth/ancestor::tei:entry//tei:cit[@type='translation']/tei:form
        else
            $orth/ancestor::tei:entry/tei:form
      return if ($onlyOrth) then $form/tei:orth else $form
};

(:~ return translations of a quote :)
declare function teidictjson:translateQuote($quote as element(tei:quote)) as node()* {
        for $q in $quote/following-sibling::tei:cit[@type="translation"]/tei:quote return
        <translation xml:lang="{$teidictjson:tolang}">{teidictjson:quoteText($q)}</translation>
};

(:~ return html help for available request parameters :)
declare
%templates:wrap
function teidictjson:paramHelp($node as node(), $model as map(*)) {
    (
  <h3>available parameters for searching (/search)</h3>,
  <ul>
    <li>searchterm (lucene syntax)</li>
    <li>searchlang: {$teidictjson:fromlang}, {$teidictjson:tolang}</li>
    <li>section: {$teidictjson:sections} (default "lemmas", alltext only works for /search)</li>
    <li>sensitive: when present and true search case and diacrit sensitive</li>
    <li>highlight: when present and true matches in search results are highlighted</li>
    <li>pos: {$teidictjson:pos}</li>
    <li>type for usage: {$teidictjson:usg}</li>
    <li>offset</li>
  </ul>
  )
};

declare variable $teidictjson:dateLastModified := if (cache:get("teidictjson:data","lastmodified")) then
cache:get("teidictjson:data","lastmodified") else
let $p := cache:put("teidictjson:data","lastmodified",
head(for $doc in $teidictjson:data/*
    let $name := util:document-name($doc)
    let $date := xmldb:last-modified($config:data-root,$name)
    order by $date descending
    return string($date)))
return cache:get("teidictjson:data","lastmodified");

(:~ return latest modification date of the data :)
declare function teidictjson:data-last-modified() as xs:string {
    $teidictjson:dateLastModified
};

(:~ show application info including software version and data-last-modified:)
declare
function teidictjson:info() as xs:string {
    teidictjson:json(),
    fn:serialize(
    <info>{
    for $line in fn:tokenize(fn:unparsed-text($config:app-root||"/build.info"),"\n")
    let $kv := fn:tokenize(fn:normalize-space($line),": ")
    where fn:string-length($kv[1])>1
    return element {$kv[1]} {$kv[2]}
    }
    <dictionary>{teidictjson:dictId()}</dictionary>
    <title>{teidictjson:title(<n/>,map{})}</title>
    <data-last-modified>{teidictjson:data-last-modified()}</data-last-modified>
    <articles>{count($teidictjson:data/tei:TEI)}</articles>
    <fromlang>{$teidictjson:fromlang}</fromlang>
    <tolang>{$teidictjson:tolang}</tolang>
    </info>, $teidictjson:jsonparams)
};

declare function teidictjson:noLeadingWildcard() {
    map:remove(teidictjson:luceneopts(),"leading-wildcard")
};

(:~ search in forms, return translations of the lemma :)
declare function teidictjson:translate($form as xs:string, $lang as xs:string) as xs:string* {
    teidictjson:json(),
    if ($form='') then () else
    fn:serialize(<json>{
        for $f in $teidictjson:data//tei:orth[ft:query(.,teidictjson:searchTerm($form),
teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]
        /ancestor::tei:form[@type="lemma"]
        return <lemma xml:lang="{$f/@xml:lang}" pos="{teidictjson:pos($f)}">{
            $f/tei:orth/text(),
            for $tr in fn:distinct-values(teidictjson:translateForm(
                $f/tei:orth,
                $lang=$teidictjson:fromlang, fn:true())/text())
            return <tr>{$tr}</tr>
            }</lemma>
            }</json>,
        $teidictjson:jsonparams)
};

declare function teidictjson:searchText($form as xs:string, $lang as xs:string) as xs:string* {
    teidictjson:searchText($form,$lang,0,10)
};

(: search in text :)
declare function teidictjson:searchText($form as xs:string, $lang as xs:string, $offset as xs:nonNegativeInteger, $max as xs:integer) as xs:string* {
    teidictjson:json(),
    let $results := if ($form='') then () else $teidictjson:data//tei:quote[
                        ft:query(.,teidictjson:searchTerm($form),
                        teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]
    let $subset := if ($max > 0) then fn:subsequence(
    $results,
    $offset,
    if ($max>$teidictjson:maxhits) then $teidictjson:maxhits else $max)
    else $results
(: TODO do we want to allow unlimited retrieval?
   TODO increase maxhits? (makes querying multiple dicts using offset/max easier)
      :)
    return fn:serialize(<json offset="{$offset}" max="{if ($max>$teidictjson:maxhits) then $teidictjson:maxhits else $max}" total="{fn:count($results)}">{
        for $q in $subset
        let $section := if ($lang=$teidictjson:fromlang) then $q/parent::tei:cit/@type else $q/../parent::tei:cit/@type
        let $entry := $q/ancestor::tei:entry
        let $lemma := $entry/tei:form/tei:orth
        let $pos := teidictjson:pos($entry)
        return <text xml:lang="{$teidictjson:fromlang}" section="{$section}" lemma="{$lemma}" pos="{$pos}">{
                teidictjson:quoteText(if ($lang=$teidictjson:fromlang) then $q else $q/../../tei:quote),
                teidictjson:translateQuote(if ($lang=$teidictjson:fromlang) then $q else $q/../../tei:quote)
            }</text>
            }</json>,
        $teidictjson:jsonparams)
};

(:~ search in usage, provide either @type or content, or both, return lemmas with usage :)
declare function teidictjson:formsByUsage($usage as xs:string?, $type as xs:string?) as xs:string* {
    teidictjson:json(),
    if ($usage='') then () else
    fn:serialize(<json>{
            for $f in if ($usage) then
                        if ($type) then
                            $teidictjson:data//tei:usg[ft:query(.,$usage, teidictjson:luceneopts())]
                                [@type=$type][ancestor::tei:form]/ancestor::tei:form[1]
                        else
                            $teidictjson:data//tei:usg[ft:query(.,$usage, teidictjson:luceneopts())]
                                [ancestor::tei:form]/ancestor::tei:form[1]
                    else
                        if ($type) then
                            $teidictjson:data//tei:usg[@type=$type][ancestor::tei:form]/ancestor::tei:form[1]
                        else
                            ()
        return <lemma xml:lang="{$f/@xml:lang}" pos="{teidictjson:pos($f)}">{
            $f/tei:orth/text(),
            $f//tei:usg
            }</lemma>
            }</json>,
        $teidictjson:jsonparams)
};

(:~ search in forms, return lemma with forms of a certain type :)
declare function teidictjson:forms($form as xs:string, $lang as xs:string, $type as xs:string) as xs:string* {
    teidictjson:json(),
    if ($form='') then () else
    fn:serialize(<json>{
        for $f in
            $teidictjson:data//tei:orth[ft:query(.,teidictjson:searchTerm($form),
               teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]
                /ancestor::tei:form[@type="lemma"][tei:form[@type=$type]]
        return <lemma xml:lang="{$f/@xml:lang}" pos="{teidictjson:pos($f)}">{
            $f/tei:orth/text(),
            $f/tei:form[@type=$type]
            }</lemma>
            }</json>,
        $teidictjson:jsonparams)
};

(:~ search in forms, return usage information of the lemma :)
declare function teidictjson:formUsage( $form as xs:string, $lang as xs:string) as xs:string* {
    teidictjson:json(),
    if ($form='') then () else
    fn:serialize(<json>{
        for $f in
            $teidictjson:data//tei:orth[ft:query(.,teidictjson:searchTerm($form),
               teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]
                    /ancestor::tei:form[@type="lemma"][.//tei:etym/tei:usg]
        return <lemma xml:lang="{$f/@xml:lang}" pos="{teidictjson:pos($f)}">{
            $f/tei:orth/text(),
            $f//tei:etym/tei:usg
            }</lemma>
            }</json>,
        $teidictjson:jsonparams)
};

(:~ search in forms, return definition of the lemma :)
declare function teidictjson:formDefinition( $form as xs:string, $lang as xs:string) as xs:string* {
    teidictjson:json(),
    if ($form='') then () else
    fn:serialize(<json>{
        for $f in
            $teidictjson:data//tei:orth[ft:query(.,teidictjson:searchTerm($form),
               teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]
                    /ancestor::tei:form[@type="lemma"][ancestor::tei:sense/tei:def]
        return <lemma xml:lang="{$f/@xml:lang}" pos="{teidictjson:pos($f)}">{
            $f/tei:orth/text(),
            $f/ancestor::tei:sense/tei:def
            }</lemma>
            }</json>,
        $teidictjson:jsonparams)
};

(:~ search in word forms, return property :)
declare function teidictjson:formProperty( $form as xs:string, $lang as xs:string, $prop as xs:string) as xs:string* {
    teidictjson:json(),
    if ($form='') then () else
    fn:serialize(<json>{
        for $f in
        $teidictjson:data//tei:orth[ft:query(.,teidictjson:searchTerm($form),
               teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]/..
            [*[fn:local-name()=$prop]][ancestor-or-self::tei:form[@type="lemma"]]
        return <form xml:lang="{$f/ancestor-or-self::tei:form[@type="lemma"]/@xml:lang}" type="{$f/@type}">{
            $f/tei:orth/text(),
            $f/*[fn:local-name()=$prop]
            }</form>
            }</json>,
        $teidictjson:jsonparams)
};

(:~ search in word forms, return grammar :)
declare function teidictjson:formGrammar( $form as xs:string, $lang as xs:string) as xs:string* {
    teidictjson:json(),
    if ($form='') then () else
    fn:serialize(<json>{
        for $f in $teidictjson:data//tei:orth[ft:query(.,teidictjson:searchTerm($form),
               teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]
            [ancestor::tei:form[@type='lemma']]/..
        return <form xml:lang="{$f/ancestor-or-self::tei:form[@type='lemma']/@xml:lang}" type="{$f/@type}">{
            $f/tei:orth/text(),
            for $g in $f/../tei:gram|$f/tei:gram|$f/../tei:gramGrp|$f/tei:gramGrp return $g
            }</form>
            }</json>,
        $teidictjson:jsonparams)
};

(:~ search in forms, return text with translations in a section for the lemma :)
declare function teidictjson:text( $form as xs:string, $lang as xs:string, $sections as xs:string+, $pos as xs:string?) as xs:string* {
    teidictjson:json(),
    if ($form='') then () else
    fn:serialize(<json>{
        let $allsects := fn:string-join($sections,"")
        for $f in
            $teidictjson:data//tei:orth[ft:query(.,teidictjson:searchTerm($form),
               teidictjson:facets($lang=$teidictjson:tolang,teidictjson:luceneopts()))]
                /ancestor::tei:form
                [@type="lemma"]
                [ancestor::tei:entry//tei:cit[fn:contains($allsects,@type)]]
        where not($pos) or ($f//tei:gram[.="pos."||$pos])
        return <lemma xml:lang="{$f/@xml:lang}" pos="{teidictjson:pos($f)}">{
            $f/tei:orth/text(),
            for $sect in $sections
                for $q in $f/ancestor::tei:entry//tei:cit[@type=$sect]/tei:quote
                return element {$sect} {
                    teidictjson:quoteText($q),
                    teidictjson:translateQuote($q)
            }}</lemma>
            }</json>,$teidictjson:jsonparams)
};

(:~ return capabilities of the dictionary :)
declare function teidictjson:capabilities( ) as xs:string* {
    teidictjson:json(),
    fn:serialize(<json>{
        for $cap in $teidictjson:capacities
        let $has := if ($teidictjson:data/tei:teiCorpus//tei:purpose[@type=$cap]) then "supported" else "support uncertain"
        return <capacity>{$cap}: {$has}</capacity>
        }</json>,$teidictjson:jsonparams)
};
(:~ add Content-Type and Content-Disposition inline header:)
declare function teidictjson:text() {
    (response:set-header('Content-Type', 'text/plain; charset=utf-8'),
    response:set-header('Content-Disposition', 'inline; filename="text.txt"'))
};
(:~ add Content-Type and Content-Disposition inline header:)
declare function teidictjson:json($section as xs:string) {
    (response:set-header('Content-Type', 'application/json; charset=utf-8'),
    response:set-header('Content-Disposition', 'inline; filename="' || $section || '.json"'))
};
declare function teidictjson:json() {
    teidictjson:json("result")
};

