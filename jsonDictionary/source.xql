xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "modules/TEIDictJson.xql";
import module namespace request="http://exist-db.org/xquery/request";

teidictjson:xmlSource(
    request:get-parameter("lemma",""),
    request:get-parameter("readable","false")="true",
    request:get-parameter("notext","false")="true"
)