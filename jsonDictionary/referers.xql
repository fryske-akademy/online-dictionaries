xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "modules/TEIDictJson.xql";
import module namespace request="http://exist-db.org/xquery/request";

teidictjson:browserCache(),
teidictjson:json(),
serialize(teidictjson:refererStrings(
    request:get-parameter("lemma","")
),$teidictjson:jsonparams)