<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                version="3.1">

    <xsl:output method="text" encoding="utf-8" />

<!--  xslt -xsl:/home/eduard/online-dictionaries/jsonDictionary/teiToTranslations.xslt -s:/home/eduard/test.xml dir=file:`pwd`/data > translations.txt  -->

    <xsl:param name="dir" as="xs:string" select="'supply dir as path'"/>
    <xsl:param name="bibl" select="false()" as="xs:boolean"/>

    <xsl:template match="/">
        <xsl:for-each select="uri-collection($dir)">
            <xsl:if test="ends-with(.,'.xml')">
                <xsl:apply-templates select="document(.)//tei:entry/tei:form[@type='lemma']"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="tei:form">
        <xsl:value-of select="tei:orth"/><xsl:text>
</xsl:text>
        <xsl:for-each select="../tei:cit[@type='translation']/tei:form|../tei:sense/tei:cit[@type='translation']/tei:form">
            <xsl:value-of select="'tr: '"/><xsl:value-of select="tei:orth"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
