<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="#all"
                version="3.0">

    <xsl:import href="readableDictXml.xslt"/>

    <xsl:template match="/">
        <xsl:apply-templates select="*"/>
    </xsl:template>

    <xsl:template match="results">
        <xsl:copy>
            <xsl:apply-templates select="@*|result"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="translation">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="result">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:form">
        <xsl:element name="{@type}">
            <xsl:apply-templates select="@*[name()!='type']|tei:orth/text()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:ref|tei:note|tei:etym|tei:bibl"/>


</xsl:stylesheet>