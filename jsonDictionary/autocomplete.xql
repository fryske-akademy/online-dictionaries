xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "modules/TEIDictJson.xql";
import module namespace request="http://exist-db.org/xquery/request";

teidictjson:autocomplete(request:get-parameter("form",""),request:get-parameter("searchlang",""),request:get-parameter("pos",""))