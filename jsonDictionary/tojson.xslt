<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:json="http://json.org/"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="#all"
                version="3.0">

    <xsl:import href="xml-to-json.xslt"/>

    <xsl:param name="use-badgerfish" as="xs:boolean" select="true()"/>
    <xsl:param name="skip-root" as="xs:boolean" select="false()"/>

</xsl:stylesheet>