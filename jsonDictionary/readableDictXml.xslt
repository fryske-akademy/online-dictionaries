<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="#all"
                version="3.0">

    <xsl:mode on-multiple-match="use-last"/>

    <xsl:template match="text()" priority="-1">
        <xsl:copy/>
    </xsl:template>

    <xsl:template match="*" priority="-1">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@*" priority="-1">
        <xsl:copy/>
    </xsl:template>

    <xsl:template match="tei:cit">
        <xsl:element name="{@type}">
            <xsl:apply-templates select="@*[not(name()='type')]|node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:quote|tei:gloss">
        <text>
            <xsl:apply-templates select="node()"/>
        </text>
    </xsl:template>

    <xsl:template match="tei:hi">
        <strong><xsl:value-of select="."/></strong>
    </xsl:template>

    <xsl:template match="tei:etym">
        <xsl:apply-templates select="*"/>
    </xsl:template>

    <xsl:template match="tei:text">
        <xsl:apply-templates select="tei:body/tei:superEntry|tei:body/tei:entry"/>
    </xsl:template>

    <xsl:template match="tei:TEI">
        <xsl:apply-templates select="tei:text/tei:body/tei:superEntry|tei:text/tei:body/tei:entry"/>
    </xsl:template>

    <xsl:template match="tei:superEntry">
        <entries>
            <xsl:apply-templates select="@*|node()"/>
        </entries>
    </xsl:template>

    <xsl:template match="tei:entry">
        <entry>
            <xsl:apply-templates select="@*|node()"/>
        </entry>
    </xsl:template>

    <xsl:template match="tei:sense">
        <sense>
            <xsl:apply-templates select="*"/>
        </sense>
    </xsl:template>

    <xsl:template match="tei:form">
        <xsl:element name="{@type}">
            <xsl:apply-templates select="@*[name()!='type']|node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:orth">
        <form><xsl:value-of select="."/></form>
    </xsl:template>

    <xsl:template match="tei:def">
        <definition>
            <xsl:apply-templates select="node()"/>
        </definition>
    </xsl:template>

    <xsl:template match="tei:ref[@target]">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:gram">
        <xsl:element name="{substring-before(.,'.')}">
            <xsl:value-of select="substring-after(.,'.')"/>
        </xsl:element>
    </xsl:template>


</xsl:stylesheet>