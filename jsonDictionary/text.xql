xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "modules/TEIDictJson.xql";
import module namespace request="http://exist-db.org/xquery/request";
import module namespace util="http://exist-db.org/xquery/util";


try {
    teidictjson:text(request:get-parameter("form",""),
    request:get-parameter("searchlang",request:get-parameter("lang",$teidictjson:fromlang)),
    request:get-parameter("type",("example","collocation","proverb")),
    request:get-parameter("pos",false())
    )
} catch * {
    util:log("debug", $err:code || ": " || $err:description || $err:line-number || ':' || $err:column-number),
        serialize(<error>try other query, or contact via frisian.eu</error>,$teidictjson:jsonparams)

}
