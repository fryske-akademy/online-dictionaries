xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "modules/TEIDictJson.xql";
import module namespace request="http://exist-db.org/xquery/request";

teidictjson:searchJSON(
    request:get-parameter("searchlang",$teidictjson:fromlang),
    request:get-parameter("searchterm",""),
    xs:positiveInteger(request:get-parameter("offset","1")),
    request:get-parameter("section","lemmas")
    )