<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:parallel="http://parallel"
                xmlns:ana="http://www.w3.org/2005/xpath-functions"
                version="3.1">

    <xsl:output method="text" encoding="utf-8" />

<!--  xslt -xsl:/home/eduard/online-dictionaries/jsonDictionary/teiToParallel.xslt -s:/home/eduard/test.xml dir=file:`pwd`/data > parallel.txt  -->

    <xsl:param name="dir" as="xs:string" select="'supply dir as path'"/>
    <xsl:param name="replace_brackets" select="true()" as="xs:boolean"/>
    <xsl:param name="bibl" select="false()" as="xs:boolean"/>

    <xsl:template match="/">
        <xsl:for-each select="uri-collection($dir)">
<!--            <xsl:message><xsl:value-of select="."/> </xsl:message>-->
<!--            <xsl:value-of select="."/><xsl:text>-->
<!--</xsl:text>-->
            <xsl:if test="ends-with(.,'.xml')">
                <xsl:apply-templates select="document(.)//tei:cit[@type!='translation']/tei:quote"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:quote" priority="-1"/>

    <xsl:template match="tei:quote[following-sibling::tei:cit[@type='translation']/tei:quote]">
<!-- Dit script gaat uit van zinnen als plain text direct onder tei:quote       -->
        <xsl:variable name="txts" select="distinct-values(parallel:parse(parallel:quote(.)))"/>
<!--  in txt kunnen [] zitten, als daar geen ... tussen staat zijn het commagescheiden varianten
      voor elke variant genereren we output
-->
        <xsl:variable name="source">
            <xsl:choose>
                <xsl:when test="$bibl and tei:bibl">||<xsl:value-of
                        select="replace(replace(normalize-space(translate(string-join(tei:bibl//text(),';'),'&#xA;&#xD;',' ')),'; ;','; '),';  ;','; ')"/></xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:if test="count($txts)>0">
            <xsl:for-each select="following-sibling::tei:cit[@type='translation']/tei:quote">
                <xsl:variable name="trs" select="distinct-values(parallel:parse(parallel:quote(.)))"/>
                <!--  in tr kunnen [] zitten, als daar geen ... tussen staat zijn het commagescheiden varianten
                      als het aantal varianten overeenkomt met het aantal varianten in txt genereren we output voor de overeenkomstige varianten
                      als in txt geen varianten zitten genereren we output met txt voor elke tr variant
                      als het aantal varianten verschilt van het aantal varianten in txt is dat een fout
                -->
                <xsl:variable name="trsource">
                    <xsl:choose>
                        <xsl:when test="$bibl and tei:bibl">||<xsl:value-of
                                select="replace(replace(normalize-space(translate(string-join(tei:bibl//text(),';'),'&#xA;&#xD;',' ')),'; ;','; '),';  ;','; ')"/></xsl:when>
                        <xsl:otherwise></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="count($txts)=1">
                        <xsl:for-each select="$trs">
                            <xsl:call-template name="print">
                                <xsl:with-param name="from" select="replace($txts,'\[.*\]','')"/>
                                <xsl:with-param name="to" select="replace(.,'\[.*\]','')"/>
                                <xsl:with-param name="source" select="$source"/>
                                <xsl:with-param name="trsource" select="$trsource"/>
                            </xsl:call-template>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="count($txts)=count($trs)">
                        <xsl:for-each select="$txts">
                            <xsl:variable name="tr" select="subsequence($trs,position(),1)"/>
                            <xsl:call-template name="print">
                                <xsl:with-param name="from" select="replace(.,'\[.*\]','')"/>
                                <xsl:with-param name="to" select="replace($tr,'\[.*\]','')"/>
                                <xsl:with-param name="source" select="$source"/>
                                <xsl:with-param name="trsource" select="$trsource"/>
                            </xsl:call-template>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message>
                            Aantal voorkomens van [] in text en vertaling ongelijk:
                            <xsl:value-of select="string-join($txts,' || ')"/>
                            <xsl:text>
</xsl:text>
                            <xsl:value-of select="string-join($trs,' || ')"/>
                        </xsl:message>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="print">
        <xsl:param name="from" as="xs:string"/>
        <xsl:param name="to" as="xs:string"/>
        <xsl:param name="source" as="xs:string" select="''"/>
        <xsl:param name="trsource" as="xs:string" select="''"/>
        <xsl:if test="not($from='' or $to='')">
            <xsl:value-of select="$from"/><xsl:value-of select="$source"/><xsl:text>
</xsl:text>
            <xsl:value-of select="concat('tr: ',$to)"/><xsl:value-of select="$trsource"/><xsl:text>
</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:function name="parallel:quote" as="xs:string">
        <xsl:param name="quote" as="element(tei:quote)"/>
        <xsl:sequence select="normalize-space(string-join($quote//text()
        [not(ancestor::tei:bibl) and not(ancestor::tei:etym) and not(ancestor::tei:note)]
        ,''))"/>
    </xsl:function>

    <xsl:function name="parallel:parse" as="xs:string*">
        <xsl:param name="text" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="$replace_brackets and contains($text,'[') and contains($text,']')">
                <xsl:variable name="parts" select="analyze-string($text,'\[[^\]]+\]')"/>
                <xsl:for-each select="$parts/ana:match">
                    <xsl:choose>
                        <xsl:when test="preceding::ana:match"/>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="contains(.,',')">
                                    <xsl:for-each select="tokenize(normalize-space(substring-before(substring-after(.,'['),']')),',')">
                                        <xsl:variable name="sentence">
                                            <xsl:apply-templates select="$parts/*">
                                                <xsl:with-param name="token" select="."/>
                                            </xsl:apply-templates>
                                        </xsl:variable>
                                        <xsl:variable name="s" select="normalize-space(string-join($sentence,''))"/>
                                        <xsl:choose>
                                            <xsl:when test="contains($s,'[') and contains($s,']')">
                                                <xsl:sequence select="parallel:parse($s)"/>
                                            </xsl:when>
                                            <xsl:when test="contains($s,'[') and not(contains($s,']'))">
                                                <xsl:message>missing ] in <xsl:value-of select="$s"/></xsl:message>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$s"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="$text"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="$replace_brackets and (contains($text,'[') and not(contains($text,']')))">
                <xsl:message>missing ] in <xsl:value-of select="$text"/></xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:template match="ana:match">
        <xsl:param name="token"/>
        <xsl:choose>
            <xsl:when test="preceding::ana:match">
                <xsl:value-of select="."/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="$token"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="ana:non-match">
        <xsl:value-of select="."/>
    </xsl:template>

</xsl:stylesheet>
