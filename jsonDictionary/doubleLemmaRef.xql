xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "modules/TEIDictJson.xql";

declare namespace tei="http://www.tei-c.org/ns/1.0";


for $t in $teidictjson:data/tei:TEI
let $lemma := teidictjson:docId($t)
return for $v in $t//tei:cit[@type='collocation']/tei:quote[contains(.,'[') and contains(.,']')]
    let $after := substring($v, string-length(substring-before($v, '['))+1)
    let $before := substring($after, 1, string-length(substring-before($after, ']')))
    return if (contains($before,$lemma)) then $lemma || ": " || $v || "\n" else ()