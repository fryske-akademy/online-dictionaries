xquery version "3.0";

import module namespace session="http://exist-db.org/xquery/session";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;

if (starts-with($exist:path,"/translate")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="translate.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/paradigm")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="formsOfType.xql">
                <add-parameter name="type" value="paradigm"/>
            </forward>
        </dispatch>
else if (starts-with($exist:path,"/pronunciation")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="formProperty.xql">
                <add-parameter name="prop" value="pron"/>
            </forward>
        </dispatch>
else if (starts-with($exist:path,"/grammar")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="formProperty.xql">
                <add-parameter name="prop" value="grammar"/>
            </forward>
        </dispatch>
else if (starts-with($exist:path,"/stress")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="formProperty.xql">
                <add-parameter name="prop" value="stress"/>
            </forward>
        </dispatch>
else if (starts-with($exist:path,"/autocomplete")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="autocomplete.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/containsText")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="containsText.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/contains")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="contains.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/referers")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="referers.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/hyphenation")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="formProperty.xql">
                <add-parameter name="prop" value="hyph"/>
            </forward>
        </dispatch>
else if (starts-with($exist:path,"/usage")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="usage.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/byUsage")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="byUsage.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/definitions")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="definitions.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/searchText")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="searchText.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/search")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="search.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/details")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="details.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/synonyms")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="formsOfType.xql">
                <add-parameter name="type" value="synonym"/>
            </forward>
        </dispatch>
else if (starts-with($exist:path,"/variants")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="formsOfType.xql">
                <add-parameter name="type" value="variant"/>
            </forward>
        </dispatch>
else if ($exist:path="/text") then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="text.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/capabilities")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="capabilities.xql"/>
        </dispatch>
else if (starts-with($exist:path,"/parallelText")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="parallelText.xql"/>
        </dispatch>
else if (matches($exist:resource, "\.(html|json|tsv|txt)$", "s")) then (
    let $resource :=
        if (contains($exist:path, "/templates/")) then
            "templates/" || $exist:resource
        else
            if (matches($exist:resource, "\.(json|tsv|txt)$", "s")) then
                $exist:resource || ".html"
            else
                $exist:resource
    let $e := if (session:exists()) then session:clear() else ()
    return
        (: the html page is run through view.xql to expand templates :)
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/{$resource}"/>
            <view>
                <forward url="modules/view.xql"/>
            </view>
            {
                if ($exist:resource = "index.html") then
            		<error-handler>
            			<forward url="error-page.html" method="get"/>
            			<forward url="modules/view.xql"/>
            		</error-handler>
                else
                    ()
            }
        </dispatch>

) else if ($exist:path eq '') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>

else if ($exist:path eq "/") then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>
else if (contains($exist:path, "/images/")) then
     <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/resources/images/{substring-after($exist:path, '/images/')}"/>
    </dispatch>
else if (starts-with($exist:path, "/xmlSource")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/source.xql"/>
    </dispatch>
else if (starts-with($exist:path, "/lex0")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/lex0.xql"/>
    </dispatch>
else if (ends-with($exist:path, "/info")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/info.xql"/>
    </dispatch>
else ()