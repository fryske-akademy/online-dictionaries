$(document).ready(function () {
    $("#spinner").hide();
    $("#spinner2").hide();
    $(document).ajaxStart(function () {
        $("#spinner2").show();
    });
    $(document).ajaxComplete(function () {
        $("#spinner2").hide();
    });
    var st = $("#searchterm");
    st.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "autocomplete.html",
                dataType: "json",
                data: {
                    q: request.term,
                    searchlang: $('#searchlang').val(),
                    pos: $('#pos').val(),
                    section: $('#section').val()
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 3,
        select: function (event,ui) {
            st.val(ui.item.value);
            $('#search').focus();//triggers change / search
        },
        change: function (event,ui) {
            if (st.val()!='') doSearch();
        }
    });
    $("#search").click(function (event) {
        doSearch();
        event.preventDefault();
    });
    var params = new URLSearchParams(window.location.search);
    var r1 = false;
    var r2 = false;
    var r3 = false;
    var r4 = false;
    var r5 = false;
    for (var field of ['theme', 'lang', 'sensitive','highlight','retrograde']) {
        if (!params.has(field)) {
            var f = $('#'+field)
            var fu = field=="theme" ? function() {r1=true;} :
                field=="lang" ? function() {r2=true;} : field=="sensitive" ? function() {r3=true;} : field=="highlight" ? function() {r4=true;} : function() {r5=true;}
            initFromStorage(f);
        }
    }
    window.setTimeout(function() {
        if (r1||r2||r3||r4||r5) {
            if (r1||r2) loadLangTheme()
            else window.location.reload();
        }
    }, 500);
    if (window.localStorage.getItem(storageKey("history"))) {
        hist = JSON.parse(window.localStorage.getItem(storageKey("history")));
        var h = $('#history');
        $.each(hist.reverse(), function (v) {
            h.append(new Option(hist[v], hist[v]));
        });
    }
    $('#tab' + $('#searchlang').val()).tab('show');
    if (st.val() > '') doSearch(); // for deep linking
    st.keypress(function (event) {
        if (event.which == 13) {
            $('#search').focus(); // should trigger change event
            event.preventDefault();
        }
    });
    $("#lang").change(function () {
        store($(this));
        loadLangTheme();
    })
    $("#sensitive").change(function () {
        store($(this))
        post();
    });
    $("#highlight").change(function () {
        store($(this))
        post();
    });
    $("#theme").change(function () {
        store($(this));
        loadLangTheme();
    });
    $("#retrograde").change(function () {
        store($(this))
        post();
    });
    for (var id of ['legal', 'contact']) {
        $('#' + id).on('shown.bs.collapse', function () {
            $(window).scrollTo($(this), 1000);
        });
    }
    $('.card-body').on('shown.bs.collapse', function () {
        $(window).scrollTo($(this), 1000);
    });
    new ClipboardJS('.copy');
    animateCollapse();
});
var animateCollapse=function () {
    $('.card-body').on('show.bs.collapse', function () {
        $(this).siblings('.card-header').addClass('active');
    });

    $('.card-body').on('hide.bs.collapse', function () {
        $(this).siblings('.card-header').removeClass('active');
    });
}
var post = function() {
    doSearch()
    $('#options').modal('hide');
}
var storageKey = function(key) {
    return window.location.pathname+"_"+key;
}
var loadLangTheme = function() {
    var l = $("#lang");
    var t = $("#theme");
    var s = "?theme=" + t.val() + "&lang=" + l.val();
    window.location.assign(s);
}
var hist = [];
var initFromStorage = function (f) {
    var t = window.localStorage.getItem(storageKey(f.prop("id")));
    if (f.is(":checkbox")) {
        if (t && t!=="false" && !f.prop("checked")) {
            f.prop("checked", true);
            return true;
        } else if (t && t==="false" && f.prop("checked")) {
            f.prop("checked", false);
            return true;
        }
    } else if (t && t !== f.val()) {
        f.val(t);
        return true;
    }
    return false;
}
var store = function (field) {
    if (field.is(":checkbox")) {
        if (field.prop("checked"))
            window.localStorage.setItem(storageKey(field.prop("id")), field.val());
        else
            window.localStorage.setItem(storageKey(field.prop("id")), "false");
    } else window.localStorage.setItem(storageKey(field.prop("id")), field.val());
}
var doSearch = function () {
    if (teidictLoading.length>0) return false;
    cls();
    // history, only store new values, independent from results
    var s = $('#searchterm');
    if (hist.find(value => value === s.val()) == undefined) {
        if (hist.length == 30) hist.shift();
        hist.push(s.val());
        window.localStorage.setItem(storageKey("history"), JSON.stringify(hist));
        var h = $('#history');
        h.empty();
        h.append(new Option('--', ''));
        $.each(hist.reverse(), function (v) {
            h.append(new Option(hist[v], hist[v]));
        });
    }
    if ($('#section').val() === 'alltext') {
        // search all categories
        _search(['lemmas', 'example', 'collocation', 'proverb']);
    } else {
        for (var id of ['lemmas', 'example', 'collocation', 'proverb']) {
            if ($('#section').val() != id) $('#' + id + 'results').collapse('hide');
        }
        _search($('#section').val());
    }
    return false;
}
var _noResults = function () {
    if ($("#structure").length == 1) return false;
    for (var id of ['example', 'collocation', 'proverb']) {
        if ($('#' + id + 'tot').text() != "0") return false;
    }
    return true;
}
var _search = function (ids) {
    var id = Array.isArray(ids) ? ids[0] : ids;
    var e = $('#' + id + 'results');
    $('#' + id + 'tot').text("0");
    var f = function () {
        var tot = $('#' + id + 'total').text();
        $('#' + id + 'tot').text(tot ? tot : 0);
        if (Array.isArray(ids) && ids.length > 1) {
            _search(ids.slice(1));
        }
    }
    $('#section').val(id);
    teidictLoad('#' + e.attr('id'), 'search.html', $('#searchform').serialize(), f);
    if (Array.isArray(ids)) $('#section').val("alltext");
}
var cls = function () {
    for (var id of ['lemmas', 'example', 'collocation', 'proverb']) {
        $('#' + id + 'results').empty();
        $('#' + id + 'tot').text(0);
    }
    $('#details').empty();
}
var fill = function (target, uri, data) {
    $.ajax(uri, {
            data: data,
            error: function (response, status, xhr) {
                showError();
            },
            success: function (data) {
                $(target).replaceWith(data);
            }
        }
    );
}
var teidictLoad = function (target, uri, data, callback) {
    if (isLoading(target)) return;
    teidictLoading.push(target);
    var f = function (response, status, xhr) {
        stopLoading(target);
        $('#spinner').hide();
        if (status === "error") {
            showError();
            return;
        }
        if (callback && typeof callback == 'function') callback(response, status, xhr);
    };
    $('#spinner').show();
    $(target).load(uri, data, f);
}
var isLoading = function(target) {
    return teidictLoading.findIndex(function(tg) {return tg==target}) != -1;
}
var stopLoading = function(target) {
    let i = teidictLoading.findIndex(function(tg) {return tg==target});
    if (i!=-1) teidictLoading.splice(i,1);
}
var teidictLoading = [];
var toResultPage = function (data, section) {
    if (teidictLoading.length>0) return;
    teidictLoad('#' + section + 'results', 'search.html', data);
}
var toPage = function handle(e, section) {
    if (e.keyCode === 13) {
        e.preventDefault();
        $('#vanaf' + section).click();
    }
}
var legal = function (legal, lang) {
    teidictLoad('#legalbody', 'legal.html', 'legal=' + legal + '&lang=' + lang)
}
var showHidePos = function (show) {
    cls();
    var pd = $('#posdrop');
    if (show) pd.show(); else {
        $('#pos').val('');
        pd.hide();
    }
}
var showError = function () {
    $('#alert').addClass("show");
    setTimeout(function () {
        $('#alert').removeClass("show")
    }, 3000);
}
var toDetails = function (data, search, id) {
    if (teidictLoading.length>0) return;
    if (id && $('#'+id).length && $('#detailModal').length) {
        $('#detailModal').scrollTo($('#'+id), 1000);
        return;
    }
    teidictLoading.push('#details');
    if ($("#detailModal").hasClass('show')) {
        $('#detailModal').on('hidden.bs.modal', function (e) {
            $('#detailModal').off('hidden.bs.modal');
            _details(data, search, id);
        });
        $('#detailModal').modal('hide');
    } else {
        _details(data, search, id)
    }
}
var _details = function (data, search, id) {
    $('#details').load('details.html', data, function (response, status, xhr) {
        stopLoading('#details');
        if (status === "error") {
            showError();
            return;
        }
        $('#detailModal').on('shown.bs.modal', function (e) {
            if (search) {
                var idattr = id && id != '' ? '#' + id : '#details';
                var look = idattr + ' .searchHitHere';
                var span = $(look).filter(function () {
                    return $(this).text().replaceAll(/[\r\n\t]*/g,'').replaceAll(/ +/g,' ').includes(search);
                }).first();
                if (span.length == 1) {
                    span.addClass("font-italic");
                    span.attr("title", "match");
                    // op zoek naar collapse parent
                    unHide(span);
                    $('#detailModal').scrollTo(span, 1000);
                }
            }
        });
        $('#detailModal').modal('show');
    });

}
var unHide = function(elem) {
    var hiddendiv = elem.closest('div.collapse').filter(function () {
        return !$(this).hasClass('show');
    }).first();
    if (hiddendiv.length == 1) {
        // show
        hiddendiv.collapse('show')
    }

}
var toggleVb = function (cardId, triangleId) {
    var triangle = $(triangleId);
    var closed = triangle.text() === '▸';
    $(cardId + ' .collapse').each(function () {
        if (closed) {
            $(this).collapse('show');
        } else {
            $(this).collapse('hide');
        }
    });
    if (closed) {
        triangle.html('&#9662;');
    } else {
        triangle.html('&#9656;');
    }
}
var printElement = function (elem, append, delimiter) {
    var domClone = elem.cloneNode(true);
    $(document.body).addClass("details")

    var $printSection = document.getElementById("printSection");

    if (!$printSection) {
        $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }

    if (append !== true) {
        $printSection.innerHTML = "";
    } else if (append === true) {
        if (typeof (delimiter) === "string") {
            $printSection.innerHTML += delimiter;
        } else if (typeof (delimiter) === "object") {
            $printSection.appendChild(delimiter);
        }
    }

    $printSection.appendChild(domClone);
    window.print();
}
var afterPrint = function () {
    $(document.body).removeClass("details");
    $("#printSection").empty();
}