xquery version "3.0";

import module namespace xmldb="http://exist-db.org/xquery/xmldb";

declare namespace validation="http://exist-db.org/xquery/validation";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace repo="http://exist-db.org/xquery/repo";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

(sm:chmod(xs:anyURI($target || "/modules/view.xql"), "rwxr-Sr-x"),
sm:chmod(xs:anyURI($target || "/modules/viewXml.xql"), "rwxr-Sr-x")
(:sm:chgrp(xs:anyURI($target || '/modules/config.xqm'),'dba'),:)
(:sm:chmod(xs:anyURI($target || '/modules/config.xqm'),'rwxrwsr-x'):)
)