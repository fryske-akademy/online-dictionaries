xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "../teidictjson/modules/TEIDictJson.xql";
declare namespace tei="http://www.tei-c.org/ns/1.0";

let $lines := fn:unparsed-text-lines("/db/apps/teidictjson/data/lemma_5000.txt")
return
string-join(for $tei in $teidictjson:data/tei:TEI
let $id := teidictjson:docId($tei)
where fn:index-of($lines, $id) > 0 and
not(exists($tei//tei:cit[@type='collocation'])) and exists($tei//tei:cit[@type='example'])
return $id,"\n")

