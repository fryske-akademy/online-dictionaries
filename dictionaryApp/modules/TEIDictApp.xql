xquery version "3.1";

module namespace teidictapp="http://www.fryske-akademy.org/TEIDictApp/1.0";
import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "../../teidictjson/modules/TEIDictJson.xql";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";

import module namespace templates="http://exist-db.org/xquery/html-templating";
import module namespace i18n="http://exist-db.org/xquery/i18n" at "lib/i18n.xql";
import module namespace xmldb="http://exist-db.org/xquery/xmldb";
import module namespace request="http://exist-db.org/xquery/request";
import module namespace response="http://exist-db.org/xquery/response";
import module namespace ft="http://exist-db.org/xquery/lucene";
import module namespace util="http://exist-db.org/xquery/util";
import module namespace console = "http://exist-db.org/xquery/console";
import module namespace cache = "http://exist-db.org/xquery/cache";
import module namespace mail="http://exist-db.org/xquery/mail";
import module namespace http="http://expath.org/ns/http-client";
import module namespace transform="http://exist-db.org/xquery/transform";

declare namespace properties="http://exist-db.org/xquery/properties";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace xi="http://www.w3.org/2001/XInclude";
declare namespace map="http://www.w3.org/2005/xpath-functions/map";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace fn="http://www.w3.org/2005/xpath-functions";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace err = "http://www.w3.org/2005/xqt-errors";

declare variable $teidictapp:props := properties:loadProperties("teidictapp.properties");

declare function teidictapp:getProperty($key as xs:string) as xs:string {
    teidictapp:getProperty($key,"")
};

declare function teidictapp:getProperty($key as xs:string, $default as xs:string) as xs:string {
    if (map:contains($teidictapp:props,$key)) then
        map:get($teidictapp:props,$key)
    else
        $default
};

(: The properties below determine the behaviour of the application, change them in teidict.properties as needed:)
declare variable $teidictapp:showFromLangParadigm := xs:boolean(teidictapp:getProperty("showFromLangParadigm","false"));
declare variable $teidictapp:showToLangParadigm := xs:boolean(teidictapp:getProperty("showToLangParadigm","false"));
declare variable $teidictapp:extraLang := teidictapp:getProperty("extraLang","");
declare variable $teidictapp:interfacelanguages := fn:tokenize(teidictapp:getProperty("interfaceLanguages","nl,en,fry"),",");
declare variable $teidictapp:jsonbaseurl := if (fn:ends-with(teidictapp:getProperty("jsonbaseurl"),"/")) then
    teidictapp:getProperty("jsonbaseurl") else teidictapp:getProperty("jsonbaseurl") || "/";
declare variable $teidictapp:showjsonurl := fn:false();
declare variable $teidictapp:expandAllResults := xs:boolean(teidictapp:getProperty("expandAllResults","false"));
(: constants :)
declare variable $teidictapp:standardparams := ("searchterm","searchlang","lang","section", "pos", "sensitive","highlight","retrograde");

declare variable $teidictapp:ct := cache:create("teidictapp",map{});

declare variable $teidictapp:teiToHtmlxslt := doc($config:app-root||"/teiToHtml.xslt");

declare variable $teidictapp:translations := if (fn:not(fn:empty(cache:get("teidictapp","translations")))) then cache:get("teidictapp","translations") else
    let $t := map:merge(
        for $l in $teidictapp:interfacelanguages
        return map{$l : fn:collection(fn:concat($config:app-root,"/resources/i18n"))//catalogue[@xml:lang = $l]})
    let $put := cache:put("teidictapp","translations",$t)
    return cache:get("teidictapp","translations");

declare variable $teidictapp:redacteuren :=
    if (fn:not(fn:empty(cache:get("teidictapp","redacteuren")))) then cache:get("teidictapp","redacteuren")
    else (let $r := cache:put("teidictapp","redacteuren",fn:distinct-values($teidictjson:data/tei:TEI/tei:teiHeader//tei:persName/text()))
    return cache:get("teidictapp","redacteuren")
    );
(:~
: a title for the dictionary
:)
declare
%templates:wrap
function teidictapp:title($node as node(), $model as map(*)) as xs:string {
    teidictapp:title()
};

declare function teidictapp:title() as xs:string {
   let $lang := fn:index-of($teidictapp:interfacelanguages,teidictapp:findParam("lang","en"))
   return if ($teidictjson:data/tei:teiCorpus//tei:title[@xml:lang=$lang]) then
        $teidictjson:data/tei:teiCorpus//tei:title[@xml:lang=$lang][1]/text()
      else
        if ($teidictjson:data/tei:teiCorpus//tei:title[1]/text()) then
            $teidictjson:data/tei:teiCorpus//tei:title[1]/text()
        else "missing teiCorpus"
};

(:~
: The id of the dictionary
:)
declare function teidictapp:dictId() as xs:string {
   $teidictjson:data/tei:teiCorpus//tei:msIdentifier/tei:idno/text()
};


(:~
: an description for the dictionary, transformed from msDesc in teiCorpus
:)
declare
%templates:wrap
function teidictapp:description($node as node(), $model as map(*)) as node() {
   let $lang := fn:index-of($teidictapp:interfacelanguages,teidictapp:findParam("lang","en"))
   return if ($teidictjson:data/tei:teiCorpus//tei:msDesc) then transform:transform($teidictjson:data/tei:teiCorpus//tei:msDesc,$teidictapp:teiToHtmlxslt,
            <parameters>
            <param name="lang" value="{$teidictapp:interfacelanguages[head($lang)]}"/>
            <param name="total" value="{count($teidictjson:data/*) - 1}"/>
            </parameters>) else <div>missing teiCorpus</div>
};


(:~
: Will be called from teidictapp:searchOrth to process hits. Returns a simple (xml) structure
: holding form, translations, pos and link.
:
:@param $orth the tei:orth containing the hit
:@param $searchlang the language that was searched in
:)
declare function teidictapp:orthResult($orth as node(), $searchlang as xs:string) as node() {
   let $toLanguage:= if ($searchlang=$teidictjson:fromlang) then $teidictjson:tolang else $teidictjson:fromlang
   let $ep := fn:count($orth/ancestor::tei:entry/preceding-sibling::tei:entry) + 1
   return
       <lemma>
          {
              if ($orth/ancestor::tei:cit/tei:note/text()) then
                <form note="{$orth/ancestor::tei:cit/tei:note/text()}">{$orth/text()}</form>
              else
                <form>{$orth/text()}</form>
          }
          <translations xml:lang="{$toLanguage}">
            {
                let $tr := teidictjson:translateForm($orth,teidictjson:isFromLang(), fn:true())
                return teidictapp:orthWithGram($tr)
            }
          </translations>
             <pos>{
                let $p := teidictjson:pos($orth)
                 return fn:substring-after($p,".")
             }</pos>
          <link>
             <lemma>{teidictjson:docIdF($orth)}</lemma>
             <entrypos>{$ep}</entrypos>
          </link>
       </lemma>

};


(:~
    returns only the first of the nodes with the same text
:)
declare function teidictapp:uniqueText($hits as node()*, $prev as node()?) {
    if (fn:empty($hits)) then () else
        let $r := fn:subsequence($hits,1,1)
        return (
            if (fn:empty($prev) or $prev/text()!=$r/text()) then $r else (),
            if (fn:count($hits)>1) then teidictapp:uniqueText(fn:subsequence($hits,2),$r) else ()
        )
};

(:~
:    function yielding navigation through results
:    @param $tot the total number of results found
:    @param $current the offset of the current page
:
:)
declare function teidictapp:pageLinks($tot as xs:integer, $current as xs:integer, $section as xs:string) as node()? {
    if ($tot > $teidictjson:maxhits) then
    <div class="row w-100 mb-2 align-items-center">
        {
            let $prev := xs:integer($current) - $teidictjson:maxhits
            let $next := xs:integer($current) + $teidictjson:maxhits
            let $last := $tot mod $teidictjson:maxhits
            return (
                <span class="d-none" id="{$section}total">{$tot}</span>,
                <div class="col-auto p-0"><button title="i18n(firstpage,eerste)"
                    onclick="{if ($prev<1) then 'return false;' else ''}toResultPage('offset=1{teidictapp:standardParams()}','{$section}');"
                class="{if ($prev<1) then 'text-muted' else ''} btn btn-link fa fa-step-backward"/></div>,
                <div class="col-auto p-0"><button title="i18n(prevpage,vorige) {$teidictjson:maxhits}"
                    onclick="{if ($prev<1) then 'return false;' else ''}toResultPage('offset={if ($prev<1) then 1 else $prev}{teidictapp:standardParams()}','{$section}');"
                class="{if ($prev<1) then'text-muted' else ''}  btn btn-link fa fa-chevron-left"/></div>,
                <div class="col-auto p-0">
                    <div class="input-group">
                        <input class="form-control form-control-sm" id="offset{$section}"
                        value="{$current}" size="6" type="number" min="1" max="{$tot}"
                        onkeypress="toPage(event,'{$section}');" style="max-width: 83px"/>
                      <div class="input-group-append">
                        <button class="btn btn-sm btn-secondary" id="vanaf{$section}" onclick="toResultPage('offset='+$('#offset{$section}').val()+'{teidictapp:standardParams()}','{$section}');" type="button"><i18n:text key="showfrom">vanaf</i18n:text></button>
                      </div>
                    </div>
                </div>,
                <div class="col-auto p-0"><button title="i18n(nextpage,volgende) {$teidictjson:maxhits}"
                    onclick="{if ($next>$tot) then 'return false;' else ''}toResultPage('offset={if ($next>$tot) then $current else $next}{teidictapp:standardParams()}','{$section}');"
                class="{if ($next>$tot) then 'text-muted' else ''} btn btn-link fa fa-chevron-right"/></div>,
                <div class="col-auto p-0"><button title="i18n(lastpage,laatste)"
                    onclick="{if ($next>$tot) then 'return false;' else ''}toResultPage('offset={$tot - (if ($last=0) then $teidictjson:maxhits else $last) + 1}{teidictapp:standardParams()}','{$section}');"
                class="{if ($next>$tot) then 'text-muted' else ''} btn btn-link fa fa-step-forward"/></div>
            )
        }
        <a href="search.tsv?offset={$current}{teidictapp:standardParams()}" class="ms-2 col fa fa-cloud-download-alt" title="i18n(download,download)"/>
    </div>
    else <span class="d-none" id="{$section}total">{$tot}</span>
};


(:~
:   request parameters to be added to generated links
:   when they where present in the current request.
:   when caching is set to false a parameter _cache=no will as well be added
:   @see $teidictapp:standardparams
:)
declare function teidictapp:standardParams() as xs:string {
    fn:concat(
        fn:string-join(
            for $param in $teidictapp:standardparams
                 return if (request:get-parameter($param,'')='') then () else (
                    '&amp;' || $param || '=' ||  xmldb:encode(request:get-parameter($param,''))
                 )
             ,''),
    fn:string(if (fn:not($teidictjson:cache)) then '&amp;_cache=no' else '&amp;_cache=yes'))
};

(:~
: find value in request or use ""
:)
declare function teidictapp:findParam($key as xs:string) as xs:string+ {
    teidictapp:findParam($key,"")
};

(:~
: find value in request or use default
:)
declare function teidictapp:findParam($key as xs:string, $default as xs:string) as xs:string+ {
    request:get-parameter($key,$default)
};

declare
%templates:default("offset", 1)
%templates:default("section", "alltext")
%templates:default("searchterm", "")
function teidictapp:search($node as node(), $model as map(*), $section as xs:string, $searchlang as xs:string?, $searchterm as xs:string, $offset as xs:integer) as node()* {
    <div class="{if ($section="lemmas") then "ms-3" else ""}" data-template="teidictapp:search">{
 if ($section="lemmas") then
    teidictapp:htmlOrthResults($searchlang,$searchterm,$offset)
 else
    teidictapp:htmlTextResults($searchlang,$searchterm,$offset,$section)
    }</div>
};

declare function teidictapp:showStructure($node as node(), $model as map(*), $lemma as xs:string, $entrypos as xs:integer) as node() {
    let $tei := teidictjson:source($lemma)
    return (
teidictjson:browserCache(),
        if ($tei) then
        <div id="structure" class="border d-none d-lg-inline-block col-lg-4 p-0 pe-1 ps-3 bg-light">{
            teidictapp:structure($entrypos,$tei,false())
        }</div> else ()
    )
};

(:~
: function that will call teidictapp:searchOrth and present results in html.
: @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
: @param $searchterm the (lucene) searchterm
: @param $offset from where do we present results to the user
:)
declare function teidictapp:htmlOrthResults($searchlang as xs:string?, $searchterm as xs:string?, $offset as xs:integer) as node()* {
    let $results := teidictapp:searchOrth($searchlang,$searchterm,$offset)
    let $tot := xs:integer($results/@total)
    let $list := fn:count(teidictapp:uniqueText($results/lemma/link/lemma,())) > 0
    return (
    teidictjson:browserCache(),
    if ($tot>0) then (
        if (fn:name($results)='error') then
            <span class="text-danger">{$results/text()}</span>
        else (

            <div class="row w-100 align-items-start">{
                let $res := $results/lemma[1]
                let $tei := if ($res/link/lemma) then teidictjson:source($res/link/lemma) else ()
                return (
                    teidictapp:pageLinks($tot,$offset, "lemmas"),
                    if ($tei) then
                    <div id="structure" class="border {if ($list) then "d-none" else ""} d-lg-inline-block col-lg-4 p-0 pe-1 ps-3 bg-light">{
                        teidictapp:structure($res/link/entrypos/text(),$tei,fn:false())
                    }</div> else ())
            }
            <div class="col-lg-8 ps-3 ps-lg-1 pe-0 col-12">{
            if ($list) then
            <table class="table-sm">
            <thead>
                <tr>
                    <th>{$searchlang}</th>
                    <th>{if ($searchlang=$teidictjson:fromlang) then $teidictjson:tolang else $teidictjson:fromlang}</th>
                </tr>
            </thead>
            {
                for $res at $p in $results/lemma
                return <tr class="{if ($p mod 2 = 0) then 'bg-light' else ''}">{
                    let $link := fn:concat(
                        "lemma=",xmldb:encode($res/link/lemma/text()),
                        "&amp;entrypos=",teidictapp:or($res/link/entrypos/text(),1),
                        teidictapp:standardParams())
                    return
                    (<td class="w-25">
                        <a href="#" onclick="fill('#structure','structure.html','{$link}')"
                        class="d-none d-lg-inline me-2 fa fa-eye" title="i18n(structure,structure)"/>
                        {  teidictapp:detailLink(
                             $res/link/lemma/text(),
                             $res/form/text(),
                             (),
                             $res/link/entrypos/text(),(),'details "' || $res/link/lemma/text() || '" (' || teidictapp:translate                                 ("pos."||$res/pos,"pos."||$res/pos) || ')',())
                        }
                       </td>,
                    <td>
                        <a href="translated.txt?lemma={$res/link/lemma/text()}" class="d-none d-lg-inline me-2 fa fa-file-alt"
                        title="{teidictapp:translate('download','download')} {teidictapp:translate($teidictjson:tolang,$teidictjson:tolang)}"/>
                    {
                           $res/translations/*
                    }</td>)
                }</tr>
            }</table> else ()
        }</div>
        </div>
        )

    ) else <span><i18n:text key="noresults">Geen resultaten</i18n:text></span>)
};


(:~
: function that will call teidictapp:searchOrth and present results in tsv.
: @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
: @param $searchterm the (lucene) searchterm
: @param $offset from where do we present results to the user
:)
declare function teidictapp:tsvOrthResults($searchlang as xs:string?, $searchterm as xs:string?, $offset as xs:integer) {
    let $results := teidictapp:searchOrth($searchlang,$searchterm,$offset)
    let $tot := xs:integer($results/@total)
    return
        if ($tot>0) then (
            if (fn:name($results)='error') then
                $results/text()
            else
                for $res in $results/lemma
                return
                    $res/form/text() || ' (' || teidictapp:translate($res/pos,$res/pos) || ')' || "&#9;" ||
                   fn:string-join($res/translations/translation//text()) || "&#10;"
        ) else teidictapp:translate("Geen resultaten","noresults")
};

(:~
: function that will call teidictapp:searchText and present results in html.
: @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
: @param $searchterm the (lucene) searchterm
: @param $offset from where do we present results to the user
: @param $section the text category to search in, see $teidictapp:textcategories
:)
declare function teidictapp:htmlTextResults($searchlang as xs:string?, $searchterm as xs:string?, $offset as xs:integer?, $section as xs:string) as node()* {
    let $results := teidictapp:searchText($searchterm,$offset,$section)
    let $tot := xs:integer($results/@total)
    let $from := teidictjson:isFromLang()
    return (
    teidictjson:browserCache(),
    if ($tot>0) then (
        if (fn:name($results)='error') then
            <span class="text-danger">{$results/text()}</span>
        else (
            teidictapp:pageLinks($tot,$offset,$section),
        if ($results/text) then (
        for $t at $pos in $results/text
        let $new := $pos = 0 or $t/@lemma != fn:subsequence($results/text,$pos - 1,1)/@lemma
        let $link := fn:concat(
                         "lemma=",xmldb:encode($t/@lemma),
                         "&amp;entrypos=",teidictapp:or($t/@entrypos,1),
                         if ($t/@id) then "&amp;id=" || $t/@id else "",
                         teidictapp:standardParams())
         let $searchText := fn:string-join($t//text()[if ($from) then not(ancestor::div[fn:contains(@class,"trans")]) else ancestor::div[fn:contains(@class,"trans")]])
        return
            <div class="{if ($new) then 'border-top' else ''}">
                {fn:string($t/@lemma)} {if ($t/@parent="" or $section="example")  then "" else
                    " (" || teidictapp:translate($t/@category,$t/@category) || ")"}:
                {teidictapp:detailLink(
                    $t/@lemma,
                    fn:replace($searchText,'[.]{3}',''),
                    (),
                    $t/@entrypos,
                    $t,
                    'details "' || $t/@lemma || '" (' || teidictapp:translate("pos."||$t/@pos,"pos."||$t/@pos) || ')',$t/@id)
                }
            </div>,<script class="uh{teidictapp:findParam('section')}" type="application/javascript">unHide($('.uh{teidictapp:findParam('section')}'))</script>) else (
                if (fn:name($results)='error') then
                    <span class="text-danger">{$results/text()}</span>
                else ()
            )
        )
    ) else <span>{teidictapp:translate("no results","noresults")}</span>)
};


(:~
: function that will call teidictapp:searchText and present results in tsv.
: @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
: @param $searchterm the (lucene) searchterm
: @param $section the text category to search in, see $teidictapp:textcategories
:)
declare function teidictapp:tsvTextResults($searchlang as xs:string?, $searchterm as xs:string?, $section as xs:string) {
    let $results := teidictapp:searchText($searchterm, 0, -1, $section)
    let $tot := xs:integer($results/@total)
    return
        if ($tot>0) then (
            if (fn:name($results)='error') then
                $results/text()
            else (),
            if ($results/text) then (
            for $t at $pos in $results/text
            return
                    fn:string($t/@lemma) || " (" || fn:string(if ($t/@parent="")  then "" else (teidictapp:translate($t/@parent,$t/@parent)) || "/")
                     || teidictapp:translate($t/@category,$t/@category) || ")&#9;" ||
                        fn:replace(fn:string-join($t//text()),'[.]{3}','') || "&#10;"
                ) else (
                    if (fn:name($results)='error') then
                        $results/text()
                    else ()
                )
        ) else teidictapp:translate("no results","noresults")
};

(:~
:@return $val or $def if $val not set
:)
declare function teidictapp:or($val,$def) {
    if ($val) then $val else $def
};

(:~
:    template function to print details of lemmas in html.
:    @param $node the node to be processed by the templating framework
:    @param $model the model to be used by the templating framework
:    @param $lemma the name of the lemma to link to
:)
declare
%templates:wrap
function teidictapp:printDetails($node as node(), $model as map(*), $lemma as xs:string*) as node()* {
    teidictjson:browserCache(),
    teidictapp:getDetails(request:get-parameter("lemma",""))
};

declare function teidictapp:getDetails($lemma as xs:string*) as node()* {
    if (fn:empty($lemma)) then () else
        for $l in $lemma
        let $tei := teidictjson:source($l)
        return if ($tei) then <div class="artikel">{
            for $e at $ep in $tei//tei:entry
            return teidictapp:openDetails($l,$ep,$tei, fn:true())
            }</div>
            else ()
};

declare function teidictapp:openDetails($lemma as xs:string, $entrypos as xs:integer, $tei as node()) as node() {
    teidictapp:openDetails($lemma,$entrypos,$tei,fn:false())
};

declare function teidictapp:openDetails($lemma as xs:string, $entrypos as xs:integer, $tei as node(), $print as xs:boolean) as node() {
    let $fromLang := teidictjson:isFromLang()
    let $search := request:get-parameter('searchterm','')
    let $entry := $tei//tei:entry[fn:position()=xs:integer($entrypos)]
    return
    <div class="container p-0" id="sense">
        <a href="#" class="noprint position-fixed text-danger mt-4 ms-n3 fa fa-chevron-up" style="z-index: 10"
        onclick="$('#detailModal').animate({{scrollTop: 0}}, 1000); return false;" title="top"/>
        <a href="#" class="noprint position-fixed text-danger mt-5 ms-n3 fa fa-chevron-down" style="z-index: 10"
        onclick="$('#detailModal').animate({{scrollTop: $('#detailModal').prop('scrollHeight')}}, 1000); return false;" title="bottom"/>
            <div>
                {if ($print) then <h6>{teidictapp:redacteur($entry)}</h6> else ()}
                <div class="small font-monospace">status: {string($tei//tei:msDesc/@status)}</div>
                <div class="p-0">
                  {teidictapp:structure($entrypos,$tei, true())}
                </div>
                <div class="p-0">
                  {teidictapp:ref($entry/tei:ref,"")}
                </div>
            </div>
            {teidictapp:showCard(teidictapp:translate("examples","examples"),
                for $ex at $p in $entry/tei:cit[@type='example']
                return teidictapp:processExample($ex,$p,$fromLang,$search),
                (), fn:true(), 1
            ),
            teidictapp:showCard(teidictapp:translate("collocations","collocations"),
                for $ex at $p in $entry/tei:cit[@type='collocation']
                return teidictapp:processCollocation($ex,$p,$fromLang,$search,$print),
                (), fn:true(), fn:true(), 1
            ),
            teidictapp:showCard(teidictapp:translate("proverbs","proverbs"),
                for $ex at $p in $entry/tei:cit[@type='proverb']
                return teidictapp:processCollocation($ex,$p,$fromLang,$search,$print),
                (), fn:true(), fn:true(), 1
            ),
            for $sense at $n in $entry//tei:sense[not(parent::tei:cit)]
            return (
            teidictapp:showCard(teidictapp:translate("examples","examples"),
                for $ex at $p in $sense/tei:cit[@type='example']
                return teidictapp:processExample($ex,$p,$fromLang,$search),
                teidictapp:definitions($sense/tei:def, fn:false()),
                fn:true(), $n + 1
                ),
            teidictapp:showCard(teidictapp:translate("collocations","collocations"),
                            for $ex at $p in $sense/tei:cit[@type='collocation']
                            return teidictapp:processCollocation($ex,$p,$fromLang,$search,$print),
                            teidictapp:definitions($sense/tei:def, fn:false()),
                            fn:true(), fn:true(),$n + 1
                ),
            teidictapp:showCard(teidictapp:translate("proverbs","proverbs"),
                            for $ex at $p in $sense/tei:cit[@type='proverb']
                            return teidictapp:processCollocation($ex,$p,$fromLang,$search,$print),
                            (),fn:true(), fn:true(), $n + 1
                )
            ),
            if (($teidictapp:showFromLangParadigm or $teidictapp:showToLangParadigm)) then (
                    teidictapp:showCard(
                teidictapp:translate("grammatica en paradigma","grammar"),
            for $f in $entry//tei:form[@type="lemma"][@xml:lang=$teidictjson:fromlang]
                     return <div class="row">{(
                         if ($teidictapp:showFromLangParadigm) then teidictapp:processForm($f) else (),
                         if ($teidictapp:showToLangParadigm) then
                            for $tf in teidictjson:translateForm($f/tei:orth,fn:true(),fn:false())
                            return teidictapp:processForm($tf)
                         else ()
                     )}</div>, (), fn:true(), 1)
            ) else (),
               let $refs := teidictapp:refererLinks(teidictjson:docId($tei))
                return
                if ($refs) then teidictapp:showCard(
                    teidictapp:translate("referrers","referrers") || " "  || $lemma,
                    $refs, (), fn:true(), 1
                ) else ()
            }
            {
            if (fn:not($print)) then (
               <a target="teidictXML" href="xmlSource?lemma={xmldb:encode($lemma)}">
                   <i18n:translate>
                        <i18n:text key="teilink">TEI bron</i18n:text>
                        <i18n:param key="lemma">{$lemma}</i18n:param>
                    </i18n:translate>
                </a>,
               <a target="lex0XML" href="{$teidictapp:jsonbaseurl}lex0?lemma={xmldb:encode($lemma)}" class="ms-2">
                   <i18n:translate>
                        <i18n:text key="teilex0">TEI Lex-0</i18n:text>
                        <i18n:param key="lemma">{$lemma}</i18n:param>
                    </i18n:translate>
                </a>
            ) else ()

            }
    </div>
};

declare function teidictapp:structure($entrypos as xs:integer, $tei as node(), $inDetails as xs:boolean) as item()* {
    let $lemma := teidictjson:docId($tei)
    return (
        <a href="translated.txt?lemma={$lemma}" class="d-none d-lg-inline me-2 fa fa-file-alt" title="i18n({$teidictjson:tolang},{$teidictjson:tolang})"/>,

        for $e at $ep in $tei//tei:entry
        let $p := teidictjson:pos($e)
        let $sameep := fn:number($entrypos)=$ep
        let $g := if ($e/tei:form/tei:gramGrp) then $e/tei:form/tei:gramGrp else $e/tei:form/tei:gram
        let $lemdisp := $lemma||"&#160;("||teidictapp:processGrammar(($g,$e/tei:form/tei:article))||")"
        return (
            if ($sameep) then "* " else (),
            teidictapp:showEtym($e/tei:form),
            if ($inDetails and fn:count($tei//tei:entry)=1) then
                <strong>{$lemdisp}</strong>
            else
                teidictapp:detailLink($lemma,$lemdisp,(),$ep, if ($sameep) then <strong>{$lemdisp}</strong> else (),teidictapp:translate("homonym","hom"),()),
            if ($e/tei:cit[@type='translation']/tei:form) then
                teidictapp:formTranslation($e,$inDetails)
            else <br/>,
            teidictapp:note($e/tei:note[not(preceding-sibling::*[not(self::tei:note)])]),
            teidictapp:ref($e/tei:ref,()),
            teidictapp:showSenses($e,$ep,$sameep,$lemma,$inDetails)
        )
    )
};

declare function teidictapp:showSenses($entry as node(), $ep as xs:integer, $sameep as xs:boolean, $lemma as xs:string, $inDetails as xs:boolean) as node()* {
    <div class="ms-3">{
        let $count := fn:count($entry//tei:sense[parent::tei:entry or parent::tei:cit[@type='sensegroup']])
        for $sense at $sp in $entry//tei:sense[parent::tei:entry or parent::tei:cit[@type='sensegroup' and parent::tei:entry]]
        let $defs := teidictapp:definitions($sense/tei:def, $inDetails)
        return (
            if (not($sense/preceding-sibling::tei:sense) and $sense/parent::tei:cit[@type='sensegroup']/tei:note) then
                    teidictapp:toHtml($sense/parent::tei:cit[@type='sensegroup']/tei:note,fn:false())
            else (),
            if ($sense/tei:note) then
                    teidictapp:toHtml($sense/tei:note,fn:true())
            else (),
            if ($sense/tei:form/tei:article/text() and fn:string-join($sense/tei:form/tei:article/text())!=fn:string-join($entry/tei:form/tei:article/text())) then
                ("(",teidictapp:toHtml($sense/tei:form/tei:article,true()),")")
            else (),
            if ($defs) then $defs else "missing definition",
            teidictapp:ref($sense/tei:ref,()),
            if ($sense/tei:cit[@type='translation']/tei:form) then
                teidictapp:formTranslation($sense,$inDetails)
            else ())
    }</div>

};



declare function teidictapp:showCard($title as xs:string, $contents, $def, $show as xs:boolean, $n as xs:integer) as node()? {
    if ($contents) then
        teidictapp:showCard($title,$contents,$def,$show,fn:false(),$n)
    else ()
};

declare function teidictapp:showCard($title as xs:string, $contents, $def, $show as xs:boolean, $voorbeelden as xs:boolean, $n as xs:integer) as node()? {
    if ($contents) then
        let $id := fn:replace($title," ","") || $n return
        <div class="card">
          <div class="card-header d-flex justify-content-center align-items-center p-0 {if ($show) then 'active' else ''}" id="heading{$id}">
              <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#detail{$id}"
                    aria-controls="detail{$id}" aria-expanded="{if ($show) then 'true' else 'false'}" >
                {$title}
              </button>
              {if ($def) then <span style="font-size: small">({$def})</span> else ()}
              {if ($voorbeelden) then
                  <button onclick="toggleVb('#detail{$id}','#triangle{$id}');" class="noprint btn btn-link">
                    <span id="triangle{$id}" class="text-primary d-inline">&#9656;</span>
                    &#160;
                    {teidictapp:translate("examples","examples")}
                  </button>
              else ()
              }
              <span class="me-5 ms-5"/>
            <h4 class="fa fa-chevron-up" data-bs-toggle="collapse" data-bs-target="#detail{$id}"
                     aria-controls="detail{$id}" aria-expanded="{if ($show) then 'true' else 'false'}"/></div>

            <div id="detail{$id}" class="collapse card-body p-0 {if ($show) then 'show' else ''}"
                  aria-labelledby="heading{$id}">
              {
                $contents
              }
            </div>
            <script type="text/javascript">animateCollapse();</script>
        </div>
    else ()
};

(:~
:    template function to present details of a sense of lemma to the user in html.
:    @param $node the node to be processed by the templating framework
:    @param $model the model to be used by the templating framework
:    @param $lemma the name of the lemma to link to
:    @param $entrypos the position of the tei:entry in the lemma xml
:)
declare
%templates:wrap
function teidictapp:htmlDetails($node as node(), $model as map(*), $lemma as xs:string, $entrypos as xs:integer) as node()+ {
    let $tei := teidictjson:source($lemma)
    return (
    teidictjson:browserCache(),
    if ($tei) then
        teidictapp:openDetails($lemma, $entrypos, $tei)
    else
      <span><i18n:text key="noresults">Geen resultaten</i18n:text></span>)
};

(:~
:    recursive function to process a tei:form[@type="lemma"] with its nested tei:form[@type!="lemma"] as html
:)
declare function teidictapp:processForm($form as node()) as node()? {
    let $dark := if ($form/@xml:lang=$teidictjson:tolang or $form/ancestor::tei:form[@xml:lang=$teidictjson:tolang]) then 'text-dark' else ''
    let $trans := if ($dark='') then "false" else "true"
    return transform:transform($form,$teidictapp:teiToHtmlxslt,
               <parameters><param name="toLangClass" value="{$dark}"/></parameters>
                        )
};

(:~
:function to process tei:etym as html
:)
declare function teidictapp:showEtym($parents as node()*) as node()* {
    if ($parents) then
        for $parent in $parents
        return
            for $etym in $parent/tei:etym
            return teidictapp:toHtml($etym,fn:true())
    else ()
};

(:~
: function to process tei:gram, tei:gramGrp and tei:article
:)
declare function teidictapp:processGrammar($gram as node()*) as xs:string {
    if ($gram[1] instance of element(tei:gramGrp)) then
        teidictapp:processGramGrp($gram)
    else fn:string-join(for $g in $gram
        order by $g
        return if ($g instance of element(tei:gram)) then
                teidictapp:translate($g/text(),$g/text())
            else
                teidictapp:translate(fn:local-name($g),fn:local-name($g)) || ": " || $g/text()
            , ", ")
};

declare function teidictapp:processGramGrp($gramGrp as node()*) as xs:string {
    fn:string-join(for $g in $gramGrp
        return teidictapp:processGrammar($g/*)
            , "; ")
};

(:~
: function to present text under tei:def (definitions) as html
:)
declare function teidictapp:definitions($def as node()*, $inDetails as xs:boolean) as node()* {
    let $entrypos := fn:count($def/ancestor::tei:entry/preceding-sibling::tei:entry) + 1
    for $d in $def
    return transform:transform($d,$teidictapp:teiToHtmlxslt,
        <parameters>
            <param name="inline" value="{not($inDetails)}"/>
            <param name="entrypos" value="{$entrypos}"/>
        </parameters>
     )
};

(:~
:   function to process example as html. Examples may be found in the lemma xml as child of tei:entry or tei:sense or under tei:cit[@type="proverb"]
:   or tei:cit[@type="collocation"]. An example may contain a tei:ref, tei:note, tei:def or tei:cit[@type="translation"].
:   @param $example the tei:cit to be processed
:   @param $fromlang when true the search is in the $teidictjson:fromlang language
:   @param $searchterm the searchterm to be highlighted
:)
declare function teidictapp:processExample($example as node(), $p as xs:integer, $fromLang as xs:boolean, $searchterm as xs:string) as node() {
   let $id := fn:replace(fn:generate-id($example),"[.]","")
    return element div {
        attribute class { "border" || (if ($p mod 2 = 0) then ' bg-light' else '')},
        if ($example/@xml:id) then
            attribute id {$example/@xml:id}
        else (),
        if ($example/preceding-sibling::*[1][self::tei:note]) then teidictapp:note($example/preceding-sibling::*[1]) else (),
        <div class="searchHitHere" id="{$id}">
            <a href="#" class="copy me-1 mt-1 float-start fa fa-copy" data-clipboard-target="#{$id}" title="i18n(copy,copy)"/>
            {
                if ($teidictapp:noHighlight) then
                    teidictapp:toHtml($example/tei:quote,fn:true())
                else
                    teidictapp:highlight( $example/tei:quote, $searchterm, fn:true() )
            }
        </div>,
        (teidictapp:ref($example/tei:ref,$example/tei:quote/text()[1]),
        teidictapp:note($example/tei:note),
        teidictapp:definitions($example/tei:def,fn:false())),
        <div class="ps-2 border-top text-dark searchHitHere">
            {
               teidictapp:citWithTrans($example/tei:cit[@type='translation'],$fromLang, $searchterm)
            }
        </div>
    }
};

declare function teidictapp:toHtml($n as node()*, $inline as xs:boolean) {
    transform:transform($n, $teidictapp:teiToHtmlxslt,
        <parameters>
            <param name="inline" value="{$inline}"/>
            <param name="lang" value="{teidictapp:findParam('lang','nl')}"/>
            {if ($n/../@xml:lang) then
                <param name="textlang" value="{$n/../@xml:lang}"/>
            else ()}
        </parameters>)
};


declare function teidictapp:highlight($toHighlight as node()?, $searchterm as xs:string?, $inline as xs:boolean) as node()* {
    if ($toHighlight) then
        let $h := if ($searchterm) then
            let $term := teidictjson:searchTerm($searchterm)
            let $hh := $toHighlight[ft:query(.,$term, teidictjson:luceneopts())]
            return if ($hh) then
                if (fn:starts-with($term,"sensitive:(")) then ft:highlight-field-matches($hh,"sensitive") else util:expand($hh)
                else $toHighlight
        else if ($searchterm) then util:expand($toHighlight) else $toHighlight
        return teidictapp:toHtml($h,$inline)
    else ()
};

(:~
:    function to process proverb as html. A proverb may contain a tei:ref, tei:note, tei:def, tei:cit[@type="translation"] or
:    tei:cit[@type="example"].
:    @param $proverb the tei:cit to be processed
:    @param $fromlang when true the search is in the $teidictjson:fromlang language
:    @param $searchterm the searchterm to be highlighted
:)
declare function teidictapp:processProverb($proverb as node(), $p as xs:integer, $fromLang as xs:boolean, $searchterm as xs:string) as node() {
    teidictapp:processCollocation($proverb,$p,$fromLang,$searchterm,fn:false())
};

(:~
:   function to process collocation as html. A collocation may contain a tei:ref, tei:note, tei:def, tei:cit[@type="translation"] or
:   tei:cit[@type="example"].
:   @param $collocation the tei:cit to be processed
:   @param $fromlang when true the search is in the $teidictjson:fromlang language
:   @param $searchterm the searchterm to be highlighted
:)
declare function teidictapp:processCollocation($collocation as node(), $p as xs:integer, $fromLang as xs:boolean, $searchterm as xs:string, $print as xs:boolean) as node() {
    <div class="border{if ($p mod 2 = 0) then ' bg-light' else ''}" id="{$collocation/@xml:id}">
        <div class="searchHitHere">
            {
            if ($teidictapp:noHighlight) then
                teidictapp:toHtml($collocation/tei:quote,fn:false())
            else
                teidictapp:highlight( $collocation/tei:quote, $searchterm, fn:false() )
            }
        </div>
        {(teidictapp:ref($collocation/tei:ref,$collocation/tei:quote/text()[1]),
            if ($collocation/tei:cit[@type='sensegroup']) then
                for $sense in $collocation/tei:cit[@type='sensegroup']/tei:sense
                return teidictapp:info($sense,$fromLang,$searchterm, $print)
            else teidictapp:info($collocation,$fromLang,$searchterm, $print)
        )}
    </div>
};

declare function teidictapp:formTranslation($entryOrSense as element(), $inDetails as xs:boolean) as element() {
        <div class="ms-2 mb-2 text-dark">{teidictapp:translate($teidictjson:tolang,$teidictjson:tolang)}: {
            let $orth := $entryOrSense/tei:cit[@type='translation']/tei:form/tei:orth
            return if ($orth) then
                if ($inDetails) then
                    teidictapp:orthWithNote($orth, $inDetails)
                else
                    (teidictapp:orthWithGram($orth),
                    teidictapp:note(teidictapp:noteForOrth($orth)))
            else "--"
        }</div>
};

declare function teidictapp:noteForOrth($orth as element(tei:orth)+) {
    for $o in $orth
    return $o/../tei:note | $o/ancestor::tei:cit/tei:note
};

(:~
:    function for presenting tei:cit[@type="translation"]/tei:quote as html, search term will be highlighted.
:)
declare function teidictapp:citWithTrans($cit as node()*, $fromLang as xs:boolean, $searchterm as xs:string) as node()* {
   for $c in $cit
   let $id := fn:replace(fn:generate-id($c),"[.]","")
   return <div id="{$id}" class="searchHitHere trans">
            <a href="#" class="copy me-1 mt-1 float-start fa fa-copy" data-clipboard-target="#{$id}" title="i18n(copy,copy)"/>
        {
        for $q in $c/tei:quote
        return if ($teidictapp:noHighlight) then
            teidictapp:toHtml($q,fn:false())
        else
            (teidictapp:highlight($q, $searchterm, fn:false()))
        }
        {teidictapp:note($c/tei:note)}
    </div>
};

declare function teidictapp:orthWithNote($orth as node()*) as node()* {
    teidictapp:orthWithNote($orth,fn:false())
};
(:~
:    function to present a tei:orth with optional notes, usg and etym in its ancestor::tei:form
:)
declare function teidictapp:orthWithNote($orth as node()*, $showGram as xs:boolean) as node()* {
   for $t at $p in $orth
   let $gs := if ($t/../tei:gramGrp) then $t/../tei:gramGrp else $t/../tei:gram
   let $g := if ($showGram) then teidictapp:processGrammar(
        ($gs,$t/../tei:article)) else ""
   return <span>{
   ( if ($p>1) then ", " else "",
    teidictapp:showEtym($t/ancestor::tei:form),
    if (teidictapp:noteForOrth($t)) then
        ($t/text(), teidictapp:note(teidictapp:noteForOrth($t)))
    else
        $t/text(),
    if ($g) then <span class="font-monospace small"> ({$g})</span> else ""
    )}</span>
};

declare function teidictapp:orthWithGram($orth as node()*) as node()* {
   for $t at $p in $orth
   let $gs := if ($t/../tei:gramGrp) then $t/../tei:gramGrp else $t/../tei:gram
   let $g := teidictapp:processGrammar(($gs,$t/../tei:article))
   return <span>{
   ( if ($p>1) then ", " else "",
        $t/text(),
        if ($g) then <span class="font-monospace small"> ({$g})</span> else ""
    )}</span>
};

(:~
:    function to present tei:def, tei:usg, tei:cit[@type="translation"] and tei:cit[@type="example"] inside
:    tei:cit[@type="collocation"] or tei:cit[@type="proverb"]
:)
declare function teidictapp:info($cit as node()?, $fromLang as xs:boolean, $searchterm as xs:string, $print as xs:boolean) as node()+ {
   let $id := fn:replace(fn:generate-id($cit),"[.]","")
   return (
        teidictapp:note(($cit/preceding-sibling::*[1][self::tei:note],$cit/tei:note)),
        teidictapp:definitions($cit/tei:def,fn:false()),
        <div class="ps-2 border-top text-dark">
            {
            teidictapp:citWithTrans($cit/tei:cit[@type='translation'],$fromLang,$searchterm)
            }
        </div>,
          if ($cit/tei:cit[@type='example']) then (
              if ($print) then () else
                  <div id="{$id}">
                    <button class="btn btn-link" type="button" data-bs-toggle="collapse" data-bs-target="#e{$id}" onclick="return false;"
                    aria-controls="e{$id}" aria-expanded="false">
                      <i18n:text key="examples">examples</i18n:text>
                    </button>
                  </div>,
              <div id="e{$id}" class="{if ($print) then '' else 'collapse'} ps-3"
               aria-labelledby="{$id}">{
                  for $ex at $p in $cit/tei:cit[@type='example']
                  return teidictapp:processExample($ex,$p,$fromLang,$searchterm)
              }</div>
          ) else ()
    )
};

(:~
:   simple text translation, uses i18n:getSelectedLanguage to determine the current language
:   and teidictapp:or to return the translation found or the default
:)
declare function teidictapp:translate($text as xs:string?, $key as xs:string) as xs:string {
    let $lang := i18n:getSelectedLanguage((),$teidictjson:fromlang)
    let $cats := map:get($teidictapp:translations,$lang)
    return (if (fn:count($cats//msg[@key=$key]) > 1) then
        util:log("debug", "found " || fn:count($cats//msg[@key=$key]) || " translations for " ||
        $lang || ":" || $key) else (),
    teidictapp:or($cats//msg[@key=$key][1],$text))
};

(:~
:    function to present a note in example, collocation or proverb as html.
:)
declare function teidictapp:note($note as node()*) as node()* {
   if ($note) then
        for $n in $note
        return teidictapp:toHtml($n,fn:true())
   else ()
};

(:~
:    function to present html links to lemmas that refer to the lemma argument.
:)
declare function teidictapp:refererLinks($lemma as xs:string) as node()* {
   for $ref in teidictjson:referers($lemma)
    let $refid := teidictjson:docId($ref)
    where $refid!=$lemma
    order by $refid
   return teidictapp:link((), $ref, $refid, $refid,())
};

(:~
:    function to present a tei:ref in example, collocation or proverb as html link to a lemma.
:    assumes the lemma is in @target
:)
declare function teidictapp:ref($references as node()*, $searchText as xs:string*) as node()* {
(: we now have tei:ref with working @target, possibly to @xml:id's

   refs have a target like target="zwart.xml#pid_680551"
:)
   if ($references) then
        for $ref in $references
            let $lemma := if (fn:contains($ref/@target,'.xml')) then
                fn:substring-before($ref/@target,'.xml')
            else if (fn:contains($ref/text(),'pid_')) then
                teidictjson:docId($ref) else $ref/text()
            let $id := fn:substring-after($ref/@target,'#')
            let $txt := $lemma
            let $zie := teidictapp:translate("zie","see") || " "
            let $elem := if ($id) then $teidictjson:data/fn:id($id) else
                $teidictjson:data//tei:form[@type="lemma"]/tei:orth[ft:query(.,'"'||$lemma||'"')]/ancestor::tei:TEI
        return
            if ($elem) then (
                teidictapp:link($elem,$ref, $zie || $txt,
                if (fn:contains($ref/@target,'.xml')) then () else $lemma,$searchText),
                <br/>)
            else
                ()
   else ()
};

(:~
    function to present a link in a div, calls teidictapp:detailLink
:)
declare function teidictapp:link($elem as node()?, $ref as node(), $txt as xs:string, $targetLemma as xs:string?, $searchText as xs:string?) as node() {
    (: try to find entry
       - ref can be a ref in this lemma or in another lemma (referring to this lemma)
       - when ref is in another lemma, targetLemma holds the lemma to link to and elem is the empty sequence
       - when ref is in this lemma, the lemma to link to is in the @target attribute of the ref
       - elem is the element the ref points to, not used when ref is in another lemma
    :)
    (: The lemma the link points to :)
    let $lemma := if ($targetLemma) then $targetLemma else fn:substring-before($ref/@target,'.xml')
    let $parents := $ref/ancestor::*[@xml:id]
    let $id := if ($targetLemma) then
                    if (fn:not(fn:empty($parents))) then
                        fn:subsequence($parents,fn:count($parents),1)/@xml:id
                    else ()
                else fn:substring-after($ref/@target,'#')
    let $entry := if ($targetLemma) then $ref/ancestor::tei:entry
              else $elem/ancestor-or-self::tei:entry
    let $entrypos := fn:count($entry/preceding-sibling::tei:entry) + 1
    return
    <span class="text-secondary searchHitHere ms-1">{
    teidictapp:detailLink($lemma, $txt,
        fn:string(if ($targetLemma) then $ref/@target else if ($searchText) then $searchText else ()),
        $entrypos,
        (),(),$id)
    }</span>
};

(:~
:    function to build a link to details of a lemma, takes care of parsing and escaping.
:    @param $lemma the name of the lemma to link to
:    @param $toDisplay the text for the link, maybe string or node()+
:    @param $ref when present, this text will be searched using javascript after showing details instead of $toDisplay
:    @param $entrypos the position of the tei:entry in the lemma xml
:    @param $highlighted the element (quote) holding highlighted text
:)
declare function teidictapp:detailLink($lemma as xs:string, $toDisplay, $ref as xs:string?, $entrypos as xs:integer?, $highlighted as node()?,$tooltip as xs:string?, $id as xs:string?) as node() {
        let $link := fn:concat(
                "lemma=",xmldb:encode($lemma),
                "&amp;entrypos=",teidictapp:or($entrypos,1),
                if ($id) then "&amp;id=" || $id else '',
                teidictapp:standardParams())
        let $t := if ($ref) then $ref else head($toDisplay)
        let $escaped := fn:normalize-space(fn:replace($t,"'","\\'"))
        return
        <span>
            <a
                onclick="toDetails('{$link}','{$escaped}','{$id}');return false;"
                title="{if ($tooltip) then $tooltip else $lemma}"
                href="details.html?{$link}">
                {if ($highlighted) then
                    $highlighted
                 else $toDisplay}
            </a>
            {
                if ($teidictapp:showjsonurl) then
                    <a href="{$teidictapp:jsonbaseurl}details.json?{fn:replace($link,'&amp;entrypos=[0-9]','')}"
                        class="ms-1 fa fa-code" title="json search" target="json"/>
                else ()
            }
        </span>


};

(:~
: retrieves lemma xml from collection
:)
declare
%templates:default("lemma", "")
function teidictapp:xmlSource($node as node(), $model as map(*), $lemma as xs:string) as item()? {
    (   teidictjson:browserCache(),
        let $tei := if ($lemma) then teidictjson:source($lemma) else ()
        return if ($tei) then $tei
        else <notFound>{$lemma}</notFound>
    )
};

(:~
: retrieves all translated text for a lemma
:)
declare function teidictapp:translatedText($lemma as xs:string) as xs:string* {
    for $x in teidictjson:source($lemma)//tei:cit[@type='translation']/tei:quote
    return fn:string-join($x//text(),'') || "&#10;"
};

(:~
 :   template function to autocomplete user input by looking for tei:orth that start with it.
:)
declare function teidictapp:autocomplete($node as node(), $model as map(*), $q as xs:string?) as xs:string {
    teidictjson:autocomplete($q,teidictapp:findParam("searchlang"),teidictapp:findParam("pos"))
};

declare function teidictapp:normalize($node as node()) as node() {
  typeswitch($node)
    case $text as text()
      return text { fn:normalize-space($text) }
    case $element as element()
      return
        element { fn:QName(fn:namespace-uri($element), fn:name($element)) } {
                  $element/@*,
                  for $child in $element/node() return teidictapp:normalize($child)
                }
    default return $node
 };

(:~
:    bottleneck function to search for word(forms), returns xml in this form:
:<pre>
    {
      "offset": "131",
      "max": "10",
      "query": "a*",
      "total": "632",
      "searchlang": "nl",
      "result": [ => see docs for teidictapp:orthResult
:</pre>
:    @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
:    @param $searchterm the (lucene) searchterm
:    @param $offset from where do we present results to the user
:)
declare function teidictapp:searchOrth($searchlang as xs:string?, $searchterm as xs:string?, $offset as xs:integer) as node() {
    let $retrograde := teidictapp:findParam("retrograde","false") = "true"
   return teidictapp:processHits(fn:sort(teidictjson:queryWords($searchlang,$searchterm),
(), function($o) {if ($retrograde) then
            codepoints-to-string(reverse(string-to-codepoints(ft:field($o,"lemma")))) else ft:field($o,"lemma")}),$searchlang,$searchterm,$offset)
};

declare function teidictapp:processHits($hits as node()*, $fl as xs:string?, $searchterm as xs:string?, $offset as xs:integer)  as node() {
    <lemmaHits offset="{$offset}" max="{$teidictjson:maxhits}" query="{$searchterm}" total="{fn:count($hits)}" searchlang="{$fl}">{
        let $retrograde := teidictapp:findParam("retrograde","false") = "true"
        for $o in teidictapp:uniqueOrths(fn:subsequence($hits, xs:integer($offset),$teidictjson:maxhits),map{})
        order by if ($retrograde) then
                 codepoints-to-string(reverse(string-to-codepoints(ft:field($o,"lemma")))) else ft:field($o,"lemma")
        return teidictapp:orthResult($o,$fl)
    }</lemmaHits>
};

(:~
    returns only the first of the orths with the same text and pos
:)
declare function teidictapp:uniqueOrths($orths as element()*, $unique as map(xs:string,element())) {
    if (fn:empty($orths)) then map:for-each($unique,function($k,$v) {$v}) else
        let $r := fn:subsequence($orths,1,1)
        let $k := $r/text()||teidictjson:pos($r)||ft:field($r,"lemma")
        return teidictapp:uniqueOrths(
            if (fn:count($orths)>1) then fn:subsequence($orths,2) else (),
            if ($k) then
            if (map:contains($unique,$k)) then $unique
            else map:put($unique,$k, $r)
            else $unique)
};

declare variable $teidictapp:noHighlight :=
    teidictjson:noHighlight() or
    fn:contains(request:get-uri(),"print.html")
    ;


(:~
:
:    @param $lemma the lemma for which translated text is retrieved
:)
declare
%templates:default("lemma", "")
function teidictapp:getTranslatedText($node as node(), $model as map(*), $lemma as xs:string) as xs:string* {
    (response:set-header('Content-Type', 'text; charset=utf-8'),
        response:set-header('Content-Disposition', 'attachment; filename=translations_' || $lemma || ".txt'"),
    teidictjson:browserCache(),
    teidictapp:translatedText($lemma))
};

(:~
:
:    template function to search in text returning tsv.
:    @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
:    @param $searchterm the (lucene) searchterm
:    @param $offset from where do we present results to the user
:    @param $section the text category to search in, see $teidictapp:textcategories
:)
declare
%templates:default("offset", 1)
%templates:default("section", "lemmas")
%templates:default("searchterm", "")
function teidictapp:searchTSV($node as node(), $model as map(*), $searchlang as xs:string?, $searchterm as xs:string?, $offset as xs:integer, $section as xs:string?) {
    if ($section="lemmas") then
        teidictapp:searchOrthTSV($searchlang, $searchterm, $offset)
    else
        teidictapp:searchTextTSV($searchlang, $searchterm, $section)
};

(:~
:
:    function to search in text returning tsv.
:    @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
:    @param $searchterm the (lucene) searchterm
:    @param $section the text category to search in, see $teidictapp:textcategories
:)
declare function teidictapp:searchTextTSV($searchlang as xs:string?, $searchterm as xs:string?, $section as xs:string?) {
    (
    response:set-header('Content-Type', 'text/tab-separated-values; charset=utf-8'),
    response:set-header('Content-Disposition', 'attachment; filename=' || $section || ".tsv")
    ,
    teidictjson:browserCache(),
    teidictapp:tsvTextResults($searchlang,$searchterm, $section))
};

(:~
:
:    function to search in lemmas returning tsv.
:    @param $searchlang the language to search in, one of teidictapp:fromlang and teidictapp:tolang
:    @param $searchterm the (lucene) searchterm
:    @param $offset from where do we present results to the user
:)
declare function teidictapp:searchOrthTSV($searchlang as xs:string?, $searchterm as xs:string?, $offset as xs:integer) {
    (
    response:set-header('Content-Type', 'text/tab-separated-values; charset=utf-8'),
    response:set-header('Content-Disposition', 'attachment; filename=lemmas.tsv'),
    teidictjson:browserCache(),
    teidictapp:tsvOrthResults($searchlang,$searchterm, $offset))
};

declare function teidictapp:searchText($searchterm as xs:string?, $offset as xs:integer, $section as xs:string) as node() {
    teidictapp:searchText($searchterm, $offset, $teidictjson:maxhits, $section)
};

(:~
:    bottleneck function to search in text, return xml in this form:
:<pre>
    {
      "total": "61",
      "offset": "1",
      "query": "haard",
      "searchlang": "nl",
      "text": [
        {
          "lemma": "trilhaardiertje",
          "entrypos": "1",
          "type": "example",
          "#text": "Dit tril&lt;b&gt;haard&lt;/b&gt;iertje leeft parasitair in een gedeelte van de dikke darm."
        },
:</pre>
:    @param $searchterm the (lucene) searchterm
:    @param $offset from where do we present results to the user
:    @param $max how many results do we present to the user (-1 no limit and offset 0)
:    @param $section the text category to search in, see $teidictapp:textcategories
:)
declare function teidictapp:searchText($searchterm as xs:string?, $offset as xs:integer, $max as xs:integer, $section as xs:string) as node() {
       let $browse := fn:normalize-space($searchterm) = "" or $searchterm = '*'
       let $tl := if (teidictjson:isFromLang()) then $teidictjson:tolang else $teidictjson:fromlang
       let $fl := if (teidictjson:isFromLang()) then $teidictjson:fromlang else $teidictjson:tolang
       let $search := if ($searchterm) then $searchterm else ""
           let $hits := if ($teidictjson:sortresults) then
                            fn:sort(teidictjson:query($section,$searchterm),(),
                                function($o) {head($o//text())})
                       else teidictjson:query($section,$searchterm)
               return
            <textHits total="{fn:count($hits)}" max="{$teidictjson:maxhits}" offset="{$offset}" query="{$searchterm}" searchlang="{if (teidictjson:isFromLang()) then $teidictjson:fromlang else $teidictjson:tolang}" searchterm="{$searchterm}">{
                for $hit in if ($max=-1) then $hits else fn:subsequence($hits,xs:integer($offset),$max)
                    let $lem := teidictjson:docIdF($hit)
                    let $id := fn:head(if ($browse) then $hit/ancestor::tei:cit[@xml:id]/@xml:id else ())
                    let $epos := fn:count(fn:head($hit/ancestor::tei:entry)/preceding-sibling::tei:entry) + 1
                    let $spos := if ($hit/ancestor::tei:sense) then
                            fn:count(fn:head($hit/ancestor::tei:sense[parent::tei:entry])/preceding-sibling::tei:sense) + 1
                        else 0
                    let $t := teidictjson:findType($hit)
                    let $parent := if ($t = "example" and $hit/ancestor::tei:cit[@type='collocation']) then "collocation" else ''
                return <text lemma="{$lem}" entrypos="{$epos}"
                    pos="{fn:substring-after(teidictjson:pos($hit),'.')}"
                    category="{$t}" parent="{$parent}" id="{$id}">{
                    let $sourcetext := if (teidictjson:isFromLang()) then $hit else $hit/../../tei:quote
                    return
                    if ($teidictapp:noHighlight ) then teidictapp:toHtml($sourcetext,fn:true())
                    else teidictapp:highlight($sourcetext, $searchterm,true()),
                   teidictapp:citWithTrans( if (teidictjson:isFromLang()) then $hit/../tei:cit[@type='translation'] else $hit/.., teidictjson:isFromLang(), $searchterm)

                }</text>
            }
            </textHits>
};

(:~
:    provides i18n label for $teidictjson:sections
:)
declare function teidictapp:labelForCat($cat as xs:string) as item()+ {
    let $key := fn:string(if ($cat="alltext" or $cat="lemmas" or $cat="grammar") then $cat else $cat||"s")
    return
    (
        <i18n:text key="search">search</i18n:text>,
        if ($cat!="lemmas") then (
        " ",
        <i18n:text key="in">in</i18n:text>) else (),
        " ",
        <i18n:text key="{$key}">{$cat}</i18n:text>
        )
};

declare
%templates:wrap
function teidictapp:langtabs($node as node(), $model as map(*)) {
   let $l := if (teidictapp:findParam("searchlang")='') then $teidictjson:fromlang else teidictapp:findParam("searchlang")
   let $from := $l=$teidictjson:fromlang
   return(
        <li class="nav-item">
            <a class="nav-link {if ($from) then 'active' else ''}" href="#" id="tab{$teidictjson:fromlang}"
                onclick="$(this).tab('show');if ($('#searchlang').val()=='{$teidictjson:tolang}') cls();$('#searchlang').val('{$teidictjson:fromlang}');">
                <h5 class="m-0 p-0"><i18n:text key="{$teidictjson:fromlang}">{$teidictjson:fromlang}</i18n:text></h5>
            </a>
        </li>,
        <li class="nav-item">
            <a class="nav-link {if ($from) then '' else 'active'}" href="#" id="tab{$teidictjson:tolang}"
                onclick="$(this).tab('show');if ($('#searchlang').val()=='{$teidictjson:fromlang}') cls();$('#searchlang').val('{$teidictjson:tolang}');">
                <h5 class="m-0 p-0">
                    <i18n:text key="{$teidictjson:tolang}">{$teidictjson:tolang}</i18n:text>
                    {if ($teidictapp:extraLang="") then () else
                    <span>&#160;(&amp; <i18n:text key="{$teidictapp:extraLang}">{$teidictapp:extraLang}</i18n:text>)</span>
                    }
                </h5>
            </a>
        </li>,
        <li class="nav-item">
            <a class="nav-link fa fa-question" href="#" data-bs-toggle="modal" data-bs-target="#help" aria-controls="help" style="outline: 0" aria-expanded="false"/>
        </li>,
        <li class="nav-item">
            <a class="nav-link fa fa-cog" href="#" id="optionsdialog" data-bs-toggle="modal" data-bs-target="#options" aria-controls="options" style="outline: 0" aria-expanded="false"/>
        </li>
        )
};

declare
%templates:default("theme","")
function teidictapp:css($node as node(), $model as map(*), $theme as xs:string) as node() {
    <link rel="stylesheet" id="bootstrapcss"
    href="resources/css-dist/bootstrap-fa{fn:replace(teidictapp:findParam("theme"),'light','')}.min.css?v=4"/>
};

(:~
:    template function to present i18n search form to users. NOTE that all functionality is handled using ajax get requests and partial page updates.
:)
declare function teidictapp:searchForm($node as node(), $model as map(*)) as node() {
   let $l := teidictapp:findParam("searchlang")
   let $in := teidictapp:findParam("section")
   return <form id="searchform" class="d-flex justify-content-center" action="search.html" method="POST">
                  <nav class="navbar justify-content-center pt-1 pb-0 ps-2 w-100 navbar-expand-md navbar-light">
                        <div class="container w-25 mb-2">
                        <div class="float-start">
                          <input type="text" size="20" id="searchterm" class=" form-control form-control-sm"
                          value="{teidictapp:findParam('searchterm')}" name="searchterm" autofocus=""
                          placeholder="i18n(searchterm,zoektekst)" title="i18n(searchhelp,lucene syntax kan gebruikt worden)"/>
                        </div>
                        {if (teidictapp:findParam("sensitive","") != "") then
                        <div class="float-start d-none d-md-block">
                            <select onchange="$('#searchterm').val($('#searchterm').val() + $(this).val());$(this).val('')"
                            class="form-select form-select-sm"
                            title="diacritics|diakrieten">
                                <option value="">--</option>
                                <option value="é">é</option>
                                <option value="ú">ú</option>
                                <option value="â">â</option>
                                <option value="ê">ê</option>
                                <option value="ô">ô</option>
                                <option value="û">û</option>
                                <option value="à">à</option>
                            </select>
                        </div>
                        else ()
                        }
                        <div class="float-start">
                            <input id="search" type="submit" class="btn btn-secondary form-control form-control-sm"
                            name="search" value="i18n(search,Zoek)"/>
                        </div>
                        <div class="float-start">
                        {
                            if ($teidictapp:showjsonurl) then
                                <a href="#" onclick="this.href='{$teidictapp:jsonbaseurl}search.json?'+$('#searchform').serialize()"
                                class="ms-1 fa fa-code" title="json search" target="json"/>
                            else ()
                        }
                        </div>
                        <input id="searchlang" name="searchlang" class="d-none" type="hidden" onchange="cls();"
                            value="{teidictapp:findParam("searchlang",$teidictjson:fromlang)}"/>
                        </div>
                  </nav>
                  <div class="modal fade" id="options" tabindex="-1" aria-labelledby="optionsdialog">
                      <div class="modal-dialog modal-lg me-2 mt-5" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button class="btn btn-secondary float-start fa fa-sync" onclick="window.location.reload(true); return false;" title="i18n(reset,reset)"/>
                                <button type="button" class="btn-close fa fa-times" data-bs-toggle="modal" data-bs-target="#options"/>
                              </div>
                              <div class="modal-body">
                                <div class="mb-2 ps-4">
                                    {
                                    if (teidictapp:findParam("sensitive","false") = "true") then
                                    <input class="form-check-input" type="checkbox" checked="checked"
                                        name="sensitive" id="sensitive" value="true"/>
                                    else
                                    <input class="form-check-input" type="checkbox" name="sensitive"
                                        id="sensitive" value="true"/>
                                    }
                                    <label class="form-check-label" for="sensitive">
                                    case and diacritics sensitive searching
                                    </label>
                                </div>
                                <div class="mb-2 ps-4">
                                    {
                                    if (teidictapp:findParam("highlight","true") = "true") then
                                    <input class="form-check-input" type="checkbox" name="highlight"
                                        id="highlight" value="true" checked="checked"/>
                                    else
                                    <input class="form-check-input" type="checkbox"
                                        name="highlight" id="highlight" value="true"/>
                                    }
                                    <label class="form-check-label" for="highlight">
                                    highlight matches
                                    </label>
                                </div>
                                <div class="mb-2 ps-4">
                                    {
                                    if (teidictapp:findParam("retrograde","false") = "true") then
                                    <input class="form-check-input" type="checkbox" name="retrograde"
                                        id="retrograde" value="true" checked="checked"/>
                                    else
                                    <input class="form-check-input" type="checkbox"
                                        name="retrograde" id="retrograde" value="true"/>
                                    }
                                    <label class="form-check-label" for="retrograde">
                                    retrograde sorting
                                    </label>
                                </div>
                                <div class="mb-2">href
                                <label for="history"><i18n:text key="history">geschiedenis</i18n:text></label>
                                <select id="history" name="history" class="form-select  form-select-sm" title="i18n(history,geschiedenis)"
                                onchange="$('#searchterm').val(this.value);$('#optionsdialog').click();">
                                    <option value="">--</option>
                                </select>
                                </div>
                                <div class="form-group">
                                <select id="section" name="section" class="form-select form-select-sm" title="i18n(section,zoek in)"
                                onchange="showHidePos(this.value==='lemmas');">
                                    {
                                        for $cat in $teidictjson:sections
                                        return if ($cat=$in) then
                                            <option value="{$cat}" selected="selected">{teidictapp:labelForCat($cat)}</option>
                                        else
                                            <option value="{$cat}">{teidictapp:labelForCat($cat)}</option>
                                    }
                                </select>
                                </div>
                                <div class="mb-2" style="display: none" id="posdrop">
                                    <label for="pos">
                                        <i18n:text key="poschoice">part of speech</i18n:text>
                                    </label>
                                    <select id="pos" name="pos" onchange="cls();"
                                    class="form-select form-select-sm">{
                                       let $pos := teidictapp:findParam("pos")
                                       return (
                                                <option value="">--</option>,
                                        for $p in $teidictjson:pos
                                        return if ($pos=$p) then
                                                <option value="{$p}" selected="selected"><i18n:text key="pos.{$p}">pos.{$p}</i18n:text></option>
                                            else
                                                <option value="{$p}"><i18n:text key="pos.{$p}">pos.{$p}</i18n:text></option>
                                        )
                                    }</select>
                                </div>
                                <div class="mb-2" id="themediv">
                                    <select id="theme" name="theme"
                                    class="form-select form-select-sm">{
                                        if (teidictapp:findParam("theme","light")="light") then (
                                            <option value="light" selected="selected">light theme</option>,
                                            <option value="dark">dark theme</option>
                                            )
                                        else (
                                            <option value="light">light theme</option>,
                                            <option value="dark" selected="selected">dark theme</option>
                                            )
                                    }</select>
                                </div>
                            <div class="mb-2">
                                <label for="lang"><i18n:text key="lang">Application language</i18n:text></label>
                                <select id="lang" name="lang" class="form-select form-select-sm">{
                                   let $lang := teidictapp:findParam("lang")
                                   return
                                    for $il in $teidictapp:interfacelanguages
                                    return if ($lang=$il) then
                                            <option value="{$il}" selected="selected"><i18n:text key="{$il}">{$il}</i18n:text></option>
                                        else
                                            <option value="{$il}"><i18n:text key="{$il}">{$il}</i18n:text></option>
                                }</select>
                            </div>
                            {(teidictapp:description($node,$model),
                               <div class="row ps-3"><div class="col">data-last-modified</div><div class="col">{teidictjson:data-last-modified()}</div></div>
                             )}
                            <footer class="footer">
                                <div >
                                    <a href="{$teidictapp:jsonbaseurl}index.html" target="json">web service</a> |
                                    <a data-bs-target="#contact" href="#" data-bs-toggle="modal" data-bs-dismiss="modal"><i18n:text key="issues">issues</i18n:text></a> |
                                    <a data-bs-target="#legal" href="#" title="disclaimer" data-bs-toggle="modal" data-bs-dismiss="modal"
                                    aria-expanded="false" aria-controls="legal"
                                    onclick="legal('disclaimer','{teidictapp:findParam("lang",fn:head($teidictapp:interfacelanguages))}');">disclaimer</a> |
                                    <a data-bs-target="#legal" href="#" title="colofon" data-bs-toggle="modal" data-bs-dismiss="modal"
                                    aria-expanded="false" aria-controls="legal"
                                    onclick="legal('colofon','{teidictapp:findParam("lang",fn:head($teidictapp:interfacelanguages))}');">colofon</a> |
                                    <a href="http://www.fryske-akademy.nl" target="www" title="copyright">
                                    Fryske Akademy © 2020
                                    </a>
                                </div>
                                <div class="w-100 mt-2 rounded py-2 bg-light d-lg-flex justify-content-end">
                                  <a href="http://universaldependencies.org/" class="me-2">
                                    <img src="resources/images/ud.png" alt="Powered by universaldependencies" style="height: 30px"/>
                                  </a>
                                  <a href="http://www.tei-c.org/" class="me-2">
                                    <img src="https://www.tei-c.org/wp-content/uploads/2016/11/powered-by-TEI.png" alt="Powered by TEI" style="height: 30px" />
                                  </a>
                                  <a href="http://www.exist-db.org" class="me-2">
                                    <img src="https://exist-db.org/exist/apps/homepage/resources/img/powered-by.svg" alt="Powered by eXistdb" style="height: 30px"/>
                                  </a>
                                  <a href="http://www.fryske-akademy.nl/" class="me-2">
                                    <img src="resources/images/Fryske-Akademy-logo2.svg" alt="Developed by Fryske Akademy" style="height: 30px"/>
                                  </a>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
            {if ($teidictapp:expandAllResults) then
                <script type="text/javascript">
                    window.setTimeout(function() &#123;
                        $('#exampleresults').collapse('show');
                        $('#collocationresults').collapse('show');
                        $('#proverbresults').collapse('show');
                    &#125;,500);
                </script>
            else ()}
        </form>
};

declare function teidictapp:redacteur($n as node()) as xs:string {
    let $red := head($n/ancestor::tei:TEI/tei:teiHeader//tei:persName/text())
    return if ($red) then " ("||$red||")" else ''
};
(:~
:    template function to present print form to users.
:)
declare function teidictapp:printForm($node as node(), $model as map(*)) as node() {
    let $redacteur := request:get-parameter("redacteur","")
    return
   <form id="printform" action="printArticles.html?v=2" class="float-start ms-4"
        onsubmit="$('#toprint').load('printArticles.html?v=2',$('#printform').serialize());return false">
        <div class="form-row">
            <div class="form-check col">
                <input class="form-check-input" type="checkbox" name="status" id="status" value="candidates"
                onchange="filter();" />
                <label class="form-check-label" for="status">
                Candidates
                </label>
            </div>
            <div class="form-check col">
                <input class="form-check-input" type="checkbox" id="examples" name="examples" value="no"
                onchange="filter()" />
                <label class="form-check-label" for="examples">
                simple
                </label>
            </div>
            <div class="form-check col">
                <select id="redacteur" name="redacteur" class="form-select form-control form-control-sm" onchange="filter();" >
                    <option value="">--</option>
                    {
                        for $r in $teidictapp:redacteuren
                        return
                            if ($r=$redacteur) then
                                <option value="{$r}" selected="selected">{$r}</option>
                            else
                                <option value="{$r}">{$r}</option>
                    }
                </select>
            </div>
        </div>
        <span class="mt-2 d-inline-block">Ctrl-click: (de)selecteer één extra, Shift-click: (de)selecteer reeks</span>
        <div class="mb-2">
            {
                let $candidates := request:get-parameter("status","candidates")="candidates"
                let $noexamples := request:get-parameter("examples","no")="no"
                let $lemmas := fn:string-join(teidictapp:findParam("lemma"),"#")
                return
                <select id="lemma" size="15" name="lemma" class="form-select form-control form-control-sm" multiple="multiple" onchange="cls();" >
                    {
                        for $x in
                        if ($redacteur!="") then
                            if ($candidates) then
                                $teidictjson:data/tei:TEI//tei:sourceDesc[.//tei:persName=$redacteur]
                                //tei:msDesc[@status="candidate"]//tei:idno
                            else
                                $teidictjson:data/tei:TEI//tei:sourceDesc[.//tei:persName=$redacteur]//tei:idno
                        else
                            if ($candidates) then
                                $teidictjson:data/tei:TEI//tei:msDesc[@status="candidate"]//tei:idno
                            else
                                $teidictjson:data/tei:TEI//tei:sourceDesc//tei:idno
                        let $red := $x/preceding::tei:persName/text()
                        let $id := $x/text()
                        let $isComplex := fn:index-of($teidictjson:complexList, $id) > 0
                        where $noexamples and fn:not($isComplex) or $isComplex
                        order by $red, $id
                        return
                        if ($lemmas=$id or
                            fn:starts-with($lemmas,$id||"#") or
                            fn:ends-with($lemmas,"#"||$id) or
                            fn:contains($lemmas,"#"||$id||"#")
                        ) then
                            <option value="{$id}" selected="selected">{$id} ({$red})</option>
                        else
                            <option value="{$id}">{$id} ({$red})</option>
                    }
                </select>
            }
        </div>
        <div class="mb-2">
            <button type="submit" class="btn btn-secondary">toon</button>
            <button type="submit" class="btn btn-secondary" onclick="window.print()">print</button>
        </div>
    </form>
};

(:~
:    template function to present i18n contact form to users.
:)
declare
%templates:wrap
function teidictapp:contactForm($node as node(), $model as map(*), $naam as xs:string?, $email as xs:string?, $tel as xs:string?, $txt as xs:string?, $token as xs:string?) as node() {
   if (fn:ends-with(fn:lower-case(request:get-uri()),"index.html")) then
       teidictapp:cForm($naam,$email,$tel,$txt)
    else
        if (fn:string-length(fn:normalize-space($naam)) > 1 and
            fn:matches($email,'^[a-zA-Z0-9.!#$%&amp;''*+/=?^_`{|}~\-]+@[a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?)*$') and
            fn:string-length(fn:normalize-space($txt)) > 10) then
                try {
                    (: now captcha....:)

                    let $url as xs:string := 'https://www.google.com/recaptcha/api/siteverify'
                    let $data as xs:string := "secret="||teidictapp:getProperty("captchakey")||"&amp;response=" || $token

                    let $resp := http:send-request(<http:request method="post" href="{$url}">
                            <http:body media-type="application/x-www-form-urlencoded">{$data}</http:body>
                        </http:request>)

                    let $ok := if (fn:parse-xml($resp[1])/xs:integer(@status) ne 200) then
                        (util:log("warn", $resp[1]),
                        fn:false())
                    else
                        let $json := fn:parse-json(util:base64-decode(string($resp[2])))
                        return if (map:get($json,"success") and map:get($json,"action")="contact" and map:get($json,"score") > 0.7) then fn:true()
                        else
                            (util:log("warn", $json),
                            fn:false())
                    return if ($ok=fn:true()) then
                        if (mail:send-email(<mail>
                            <from>{teidictapp:getProperty("from")}</from>
                            <bcc>{teidictapp:getProperty("bcc")}</bcc>
                            <to>{$email}</to>
                            <subject>contact dictionary {$teidictjson:fromlang}-{$teidictjson:tolang}</subject>
                            <message><text>Uw bericht is doorgestuurd naar het team van het Online Nederlands-Fries Woordenboek.

naam: {$naam}
email: {$email}
tel: {$tel}
text:
{$txt}
                            </text></message></mail>,teidictapp:getProperty("smtp"),"utf-8")) then
                                <h5 class="text-success">mail sent....
                                <script type="text/javascript">window.setTimeout(function()&#123; location.replace("index.html") &#125;, 2000);</script>
                                </h5>
                        else
                            <h5 class="text-danger">mail failed</h5>
                    else
                        <h5 class="text-danger">mail not sent</h5>
            } catch * {
            (util:log("warn", $err:code || ": " || $err:description || $err:line-number || ':' || $err:column-number),
                <h5 class="text-danger">mail not sent</h5>)
            }
        else
            <div>
                <div class="text-danger text-center"><i18n:text key="checkfields">Check your input</i18n:text></div>
                {teidictapp:cForm($naam,$email,$tel,$txt)}
            </div>
};

declare function teidictapp:cForm($naam as xs:string?, $email as xs:string?, $tel as xs:string?, $txt as xs:string?) as node() {
   <form id="contactForm" action="contact.html" method="post">
        <p>Contact</p>
          <input type="hidden" id="token" name="token" />
        <div class="mb-2">
            <label class="d-sm-none d-xs-inline" for="naam">(2 - 50)</label>
          <input type="text" required="" pattern=".&#123;2,&#125;" maxlength="50" title="2 - 50" id="naam" class=" form-control form-control-sm" value="{$naam}" name="naam" placeholder="i18n(name,name)"/>
        </div>
        <div class="mb-2">
          <input type="email" required="" maxlength="50" id="email" class=" form-control form-control-sm" value="{$email}" name="email" placeholder="i18n(email,email)"/>
        </div>
        <div class="mb-2">
          <input type="tel" id="tel" class=" form-control form-control-sm" value="{$tel}" name="tel" placeholder="tel"/>
        </div>
        <div class="mb-2">
            <label class="d-sm-none d-xs-inline" for="txt">(10 - 500)</label>
          <textarea required="" pattern=".&#123;10,&#125;" maxlength="500" title="10 - 500" class="form-control form-control-sm" id="txt" name="txt" rows="5">{$txt}</textarea>
        </div>
        <div class="mb-2">
            <button type="submit" class="btn btn-secondary"><i18n:text key="verstuur">verstuur</i18n:text></button>
        </div>
        <div class="small">This site is protected by reCAPTCHA and the Google
    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
    <a href="https://policies.google.com/terms">Terms of Service</a> apply.</div>
    </form>
};

declare function teidictapp:getFkw($lemma as xs:string, $pos as xs:string) as node()? {
    fn:json-to-xml(fn:serialize(
            fn:json-doc("https://web2.fa.knaw.nl/foarkarswurdlist-ws/rest/find/lemma/"||$lemma||"/"||$pos),
            $teidictjson:jsonparams))
};

declare function teidictapp:showSynonyms($fkwXml as node()) as xs:string {
    fn:string-join($fkwXml//*[@key="synonym"]//text(),", ")
};

declare function teidictapp:showDutchisms($fkwXml as node()) as xs:string {
    fn:string-join($fkwXml//*[@key="dutchism"]/*/*[@key="form"]/text(),", ")
};

declare
%templates:wrap
function teidictapp:help($node as node(), $model as map(*)) as node() {
    <xi:include href="{$config:app-root}/help-{teidictapp:findParam('lang',fn:head($teidictapp:interfacelanguages))}.xml"/>
};

declare
%templates:wrap
function teidictapp:legal($node as node(), $model as map(*)) as node() {
    <xi:include href="{$config:app-root}/{teidictapp:findParam('legal','disclaimer')}-{teidictapp:findParam('lang',fn:head($teidictapp:interfacelanguages))}.xml"/>
};
