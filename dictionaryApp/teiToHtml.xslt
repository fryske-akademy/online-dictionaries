<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:i18n="http://exist-db.org/xquery/i18n"
                exclude-result-prefixes="#all"
                version="3.0">
    <xsl:output method="xhtml"/>

    <xsl:param name="inline" select="false()" />
    <xsl:param name="toLangClass" select="''"/>
    <xsl:param name="entrypos" select="1"/>
    <xsl:param name="lang" select="'en'"/>
    <xsl:param name="total" select="0"/>
    <xsl:param name="ref" select="''"/>
    <xsl:param name="textlang" select="''"/>

    <xsl:template match="tei:hi|*[local-name()='match']">
        <xsl:if test="text()">
            <strong>
                <xsl:apply-templates/>
            </strong>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:p">
        <div class="m-1">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:msDesc">
        <div>
            <xsl:apply-templates select="if (tei:p[@xml:lang=$lang]) then tei:p[@xml:lang=$lang] else tei:p"/>
            <xsl:apply-templates select="tei:msIdentifier"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:quote">
        <div>
            <xsl:call-template name="langTitle"><xsl:with-param name="noot" select="."/></xsl:call-template>
            <xsl:if test="$inline=true()"><xsl:attribute name="class" select="'d-inline'"/></xsl:if>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:msIdentifier">
        <div class="container">
            <xsl:for-each select="*">
                <div class="row">
                    <div class="col"><xsl:value-of select="local-name()"/>: </div>
                    <div class="col"><xsl:value-of select="."/></div>
                </div>
            </xsl:for-each>
            <div class="row">
                <div class="col">Total: </div>
                <div class="col"><xsl:value-of select="$total"/></div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="tei:q">
        <xsl:if test="text()">
            <q class="fst-italic">
                <xsl:apply-templates/>
            </q>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:label">
        <label>
            <xsl:apply-templates/>
        </label>
    </xsl:template>

    <xsl:template name="langTitle">
        <xsl:param name="noot"/>
        <xsl:choose>
            <xsl:when test="$textlang!=''">
                <xsl:attribute name="title">i18n(<xsl:value-of select="$textlang"/>, <xsl:value-of select="$textlang"/>)</xsl:attribute>
            </xsl:when>
            <xsl:when test="$noot/@xml:lang">
                <xsl:attribute name="title">i18n(<xsl:value-of select="$noot/@xml:lang"/>, <xsl:value-of select="$noot/@xml:lang"/>)</xsl:attribute>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:note">
        <div class="text-monospace small d-inline">
            <xsl:call-template name="langTitle"><xsl:with-param name="noot" select="."/></xsl:call-template>
            (<xsl:apply-templates/>)
        </div>
    </xsl:template>

    <xsl:variable name="apos">'</xsl:variable>

    <xsl:template match="tei:ref">
        <xsl:variable name="lemma" select="if ($ref!='') then $ref else substring-before(substring-before(@target,'#'),'.xml')"/>
        <xsl:variable name="esclemma" select="replace($lemma,$apos,concat('\\',$apos))"/>
        <xsl:variable name="id" select="substring-after(@target,'#')"/>
        <xsl:variable name="data" select="concat('lemma=',$lemma,'&amp;entrypos=',$entrypos,'&amp;lang=',$lang,
            if ($id) then concat('&amp;id=',$id) else '')"/>
        <a class="ms-1" href="details.html?{$data}">
            <xsl:attribute name="onclick">
                <xsl:value-of select="'toDetails('"/>
                <xsl:value-of select="$apos"/>
                <xsl:value-of select="$data"/>
                <xsl:value-of select="$apos"/>
                <xsl:value-of select="','"/>
                <xsl:value-of select="$apos"/>
                <xsl:value-of select="$esclemma"/>
                <xsl:value-of select="$apos"/>
                <xsl:value-of select="','"/>
                <xsl:value-of select="$apos"/>
                <xsl:value-of select="$id"/>
                <xsl:value-of select="$apos"/>
                <xsl:value-of select="');return false'"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </a>
    </xsl:template>

    <xsl:template match="tei:def">
        <div class="text-monospace small searchHitHere {if ($inline=true()) then 'd-inline' else ''} ms-1">
            <xsl:call-template name="langTitle"><xsl:with-param name="noot" select="."/></xsl:call-template>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:bibl"/>

    <xsl:template match="tei:gloss">
        <div class="{if ($inline=true()) then 'd-inline' else ''} ms-2">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:etym">
        <div class="d-inline pr-1 ms-1 me-1">
            <xsl:if test="@type">
                <xsl:attribute name="title">i18n(<xsl:value-of select="@type"/>,<xsl:value-of select="@type"/>)</xsl:attribute>
            </xsl:if>
            <xsl:for-each select="tei:usg">
                <span class="text-monospace small"
                      title="i18n({concat('usg.',./@type)},{./@type}) {./@subtype}">(<xsl:value-of select="string-join(./text(),',')"/>)</span>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="tei:form">
        <div class="pl-3 {if (@type='lemma') then 'col-md' else ''} {$toLangClass} {if (parent::tei:form) then '' else 'border'}">
            <xsl:attribute name="title">
                <i18n:text key="@type"><xsl:value-of select="@type"/></i18n:text>
                <xsl:if test="@xml:lang">&#160;<xsl:value-of select="@xml:lang"/></xsl:if>
            </xsl:attribute>
            <xsl:if test="@namekind">
                <span class="ms-1 text-monospace small"><i18n:text key="{@namekind}"><xsl:value-of select="@namekind"/></i18n:text>:</span>
            </xsl:if>
            <xsl:if test="@type='variant' or @type='synonym'">
                <span class="ms-1 text-monospace small"><i18n:text key="{@type}"><xsl:value-of select="@type"/></i18n:text>:</span>
            </xsl:if>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:pron|tei:stress">
        <span title="i18n({local-name()},{local-name()})" class="ms-1 text-monospace small">[<xsl:value-of select="text()"/>]</span>
    </xsl:template>

    <xsl:template match="tei:hyph">
        <span class="ms-1 text-monospace small">(<xsl:value-of select="text()"/>)</span>
    </xsl:template>

    <xsl:template match="tei:article">
        <span class="ms-1 text-monospace small"><i18n:text key="article">article</i18n:text>: <xsl:value-of select="."/></span>
    </xsl:template>

    <xsl:template match="tei:gram|@namekind">
        <span class="ms-1 text-monospace small"><i18n:text key="{.}"><xsl:value-of select="."/></i18n:text></span>
    </xsl:template>

    <xsl:template match="tei:gramGrp">
        <span class="ms-2"><xsl:apply-templates/></span>
    </xsl:template>

</xsl:stylesheet>
