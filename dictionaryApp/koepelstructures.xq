xquery version "3.1";

import module namespace teidictjson="http://www.fryske-akademy.org/TEIDictJson/1.0" at "../teidictjson/modules/TEIDictJson.xql";
import module namespace teidictapp="http://www.fryske-akademy.org/TEIDictApp/1.0" at "modules/TEIDictApp.xql";
import module namespace ft="http://exist-db.org/xquery/lucene";
declare namespace tei="http://www.tei-c.org/ns/1.0";

for $tei in $teidictjson:data/tei:TEI[count(//tei:entry)>1]
return teidictapp:structure(1, $tei, true())

