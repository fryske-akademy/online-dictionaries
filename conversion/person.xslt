<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="#all"
                version="3.0">

    <xsl:template match="tei:gram/text()[.='person.first']">person.1</xsl:template>

    <xsl:template match="tei:gram/text()[.='person.second']">person.2</xsl:template>

    <xsl:template match="tei:gram/text()[.='person.third']">person.3</xsl:template>

    <xsl:template match="tei:TEI[not(@linguisticsversion)]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="linguisticsversion" select="'2'"/>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template priority="-1" match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>