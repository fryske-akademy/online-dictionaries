# npm / sass building resources

- `cd dictionaryApp/resources`
- `rm -rf package-lock.json node_modules/*`
- update libs and deps in package.json
  - update with care and test!
  - **TODO** sometimes bootstrap is missing under node_modules after npm clean-install and ant xar build?
- optionally adapt `scss/teidict.scss` and `scss/teidictdark.scss`
- `npm install`)