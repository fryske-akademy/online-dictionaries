# docker setup for corpora editing environment

### create file 'adminpw' to change admin password during build:
```
let $pw1 := sm:passwd('admin','xxxx')
let $pw1 := sm:passwd('tei','xxxx')
return "changed"
```

## environment
```bash
export APPNAME=editdict
export VERSION=xxx
export VOLUMEROOT=${HOME}/volumes
export DOCKER_BUILDKIT=1
```
## prepare applications (.xar)
- download and copy [dictionaries xar](../dictionaries/build) to the docker directory

## build image:
```
./init.sh
rm adminpw
```

## run image
NOTE: see hard coded JAVA_TOOL_OPTIONS in docker-compose!
```
once: copy conf.xml to ${VOLUMEROOT}/${APPNAME}/config/
once: copy xsd to ${VOLUMEROOT}/${APPNAME}/
once: adapt the config files
once: docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
docker stack deploy --compose-file docker-compose.yml ${APPNAME}
upload dictionary articles you want to edit via webdav,  web interface or https://github.com/eXist-db/xst
```
Optionally an apache virtual host in front (proxying on uri won't work!):
```xml
<VirtualHost *:443>
    ServerName xxx
    DocumentRoot "/usr/local/apache2/htdocs"
    ProxyPass "/" http://xxx:xxx/ nocanon
    ProxyPassReverse "/" "http://xxx:xxx/
</VirtualHost>

```

### usefull commands

```
docker container|service|image|stack ls
docker service|container logs (-f) ${APPNAME}
docker stack rm ${APPNAME}
docker exec -it <container> "/bin/bash"
```
