# Home of TEI/UD based (translation) dictionaries

This project greatly simplifies the publication of dictionaries. All you need to do is produce dictionary articles in the format of this project. For this the project yields 3 artifacts:

- a teidictionaries library providing validation, Jaxb classes and to / from Xml methods, is in maven central
- a dockerized exist-db app providing json services
- a dockerized exist-db app providing a bootstrap based web application

The basis is a [TEI customization for dictionaries](src/main/resources/odd/tei_dictionaries.odd).

The web application depends on the services application, you can choose to install only the services or both. For both scenarios a docker setup is provided.

Validation of articles isn't done by the application by default. Validation is assumed to be done outside of the application in for example an editing or publishing process. Note that for validation the java library indicates runtime dependencies.

Commandline validation is cumbersome at the moment, easiest usage of the library is via a maven dependency.

The web application is themable using bootstrap and internationalized (english, dutch and frisian provided).
Both applications sync (article) data from a directory into their data collection. The syncing enables you to
separate editing and publication processes, just put reviewed and validated articles in the directory that is to be synced to the dictionary collection.

The Json services are kept stable and can be used for development of user interfaces using technologies
preferred in your own environement.

## preparing oxygen for editing dictionary xml

In order to benefit from documentation, dropdowns for values and automatic validation during editing you can customize oxygen.

- start oxygen, open preferences
- click "document type association", choose "TEI P5", click edit
- choose "schema", use "tei_dictionaries.rng" from this project
- click ok
- click choose "validation", select or add "TEI P5", click edit
- if asked confirm extending and make the extension standard
- double click in the "schema" field, use "tei_dictionaries.rng" from this project
- if necessary check "automatic validation"
- click ok two times

You can save your validated dictionary xml to the directory that the application(s) use for syncing.

## TEI Lex-0

The project resembles [TEI-lex0](https://dariah-eric.github.io/lexicalresources/pages/TEILex0/TEILex0.html), but is more restrictive, which enhances stable tool development and offers more "functions", such as capabilities of dictionaries (can it be used for...), proverbs, collocations, a namekind attribute for forms (placename, personname, plantname,...) and several schematron checks that facilitate tool development. The main structure of this projects format is a teiCorpus document with a teiHeader containing general dictionary information and for each entry / article a TEI element (or document) with a teiHeader and entry elements whereas Lex-0 organizes the dictionary as a TEI element holding (nested) entry elements.

This project offers support for TEI Lex-0 via the following functions.

- a /lex0?lemma= service endpoint to retrieve xml in TEI Lex-0 format for a lemma
- an xslt stylesheet to convert xml in this customized TEI format into TEI Lex-0
- Java methods to validate Lex-0 using xsd, rng and or schematron
- Java methods to transform xml in this customized TEI format into TEI Lex-0

Note that for validation and transformation the java library indicates runtime dependencies.

A maven build pipeline generates Java support directly from TEI Lex-0 odd using a fixed version of the TEI stylesheets. This enhances stability and reliability of the functionality.

## build and run the exist-db applications

Both the Json services and the web application are completely dockerized.

- [Json docker setup](dockerjson/README.md)
- [Web application docker setup](dockerapp/README.md)

## NOTE implicit validation in exist-db only supports xsd, not rng with schematron
You may want to convert rng with schematron to xsd 1.1 for validation in exist-db

## jmeter

[jemeter](jmeter.md)
