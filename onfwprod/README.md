# docker setup for dictionary web application

### create file 'adminpw' to change admin password during build:
```
sm:passwd('admin','xxxx')
```

## environment
```bash
export APPNAME=onfwprod
export VERSION=xxx
export VOLUMEROOT=${HOME}/volumes
export DOCKER_BUILDKIT=1
```
## prepare applications (.xar)
- adapt disclaimer, privacy, colofon and help files in dictionaryApp
- ```cd dictionaryApp/resources; npm install``` ([see buildgui.md](buildgui.md))
- build services xar: ```cd jsonDictionary; ant```
- build app xar: ```cd dictionaryApp; ant```
- copy xar's to the docker directory

## build image:
In the Dockerfile you find optional arguments
```
docker build --secret id=adminpw,src=adminpw -t ${APPNAME}:${VERSION} .
rm adminpw
```
This build will have the admin password changed.

## run image
NOTE: see hard coded JAVA_TOOL_OPTIONS in docker-compose!
```
once: copy teidictapp.properties and conf.xml to ${VOLUMEROOT}/${APPNAME}app/config/
once: copy teidictjson.properties to ${VOLUMEROOT}/${APPNAME}json/config/
once: adapt the config files
once: docker secret create ${APPNAME}app.properties ${VOLUMEROOT}/${APPNAME}app/config/teidictapp.properties
once: docker secret create ${APPNAME}json.properties ${VOLUMEROOT}/${APPNAME}json/config/teidictjson.properties
once: docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
docker stack deploy --compose-file docker-compose.yml ${APPNAME}
```
## sync (create/update/delete) data
- see [publish app](https://bitbucket.org/fryske-akademy/dws-configuration)

### usefull commands

```
docker container|service|image|stack ls
docker service|container logs (-f) ${APPNAME}
docker stack rm ${APPNAME}
docker exec -it <container> "/bin/bash"
```
